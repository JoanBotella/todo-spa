# ToDo SPA

A simple ToDo single page application, built using just vanilla technologies: HTML, JavaScript and CSS.

## ToDo

[] Each time a persister makes any change to the DB, it should also update the meta lastChange date.

[] Add a translation system.

## License

GNU GPL v3 or later https://www.gnu.org/licenses/gpl-3.0.en.html

## Author

Joan Botella Vinaches https://joanbotella.com
