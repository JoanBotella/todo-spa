
const TodoSpa_App = function (
	databaseMigrator,
	routeIdGuesser,
	router
)
{

	this.run = function ()
	{
		router.setupOnPopState();

		const routeId = routeIdGuesser.guess();

		router.route(routeId);
	}

	databaseMigrator.migrateIfRequired();
}
