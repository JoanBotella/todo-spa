
const TodoSpa_StorageContainer = function ()
{
	const ITEM_ID = 'todoSpa';

	this.get = function ()
	{
		let item = localStorage.getItem(ITEM_ID);
	
		if (item === null)
		{
			item = '{}';
			localStorage.setItem(ITEM_ID, item);
		}

		return JSON.parse(item);
}

	this.set = function (value)
	{
		localStorage.setItem(
			ITEM_ID,
			JSON.stringify(value)
		);
	}

}
