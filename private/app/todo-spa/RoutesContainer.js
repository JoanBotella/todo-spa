
const TodoSpa_RoutesContainer = function (
	serviceContainer
)
{

	let routes;

	this.get = function ()
	{
		if (routes === undefined)
		{
			routes = this._build();
		}
		return routes;
	}

		this._build = function ()
		{
			return [
				serviceContainer.getBookFrontPageHomeRoute(),
				serviceContainer.getBookFrontPageTodoListRoute(),
				serviceContainer.getBookFrontPageTodoAddRoute(),
				serviceContainer.getBookFrontPageTodoEditRoute(),
				serviceContainer.getBookFrontPageTaskListRoute(),
				serviceContainer.getBookFrontPageTaskAddRoute(),
				serviceContainer.getBookFrontPageTaskEditRoute(),
				serviceContainer.getBookFrontPageCategoryListRoute(),
				serviceContainer.getBookFrontPageCategoryAddRoute(),
				serviceContainer.getBookFrontPageCategoryEditRoute(),
				serviceContainer.getBookFrontPageSettingsShowRoute(),
				serviceContainer.getBookFrontPageSettingsEditRoute(),
				serviceContainer.getBookFrontPageDataMenuRoute(),
				serviceContainer.getBookFrontPageDataEditRoute(),
				serviceContainer.getBookFrontPageDataUploadRoute(),
				serviceContainer.getBookFrontPageDataDownloadRoute(),
				serviceContainer.getBookFrontPageAboutRoute(),
				serviceContainer.getBookFrontPageNotFoundRoute(),
			];
		}

}