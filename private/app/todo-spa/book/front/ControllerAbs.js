
const TodoSpa_Book_Front_ControllerAbs = function (
	routeArgsBuilder,
	slotFiller,
	layoutWidget
)
{
	TodoSpa_Book_ControllerAbs.call(
		this,
		routeArgsBuilder,
		slotFiller
	);

	this._buildLayoutWidgetElement = function ()
	{
		return layoutWidget.render(
			this._buildBreadcrumbs(),
			this._buildMainWidgetElement()
		);
	}

		this._buildBreadcrumbs = function ()
		{
			throw new Error('Not implemented');
		}

		this._buildMainWidgetElement = function ()
		{
			throw new Error('Not implemented');
		}

}
