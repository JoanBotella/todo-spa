
const TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb = function (
	text
)
{
	let url;

	this.getText = function ()
	{
		return text;
	}

	this.hasUrl = function ()
	{
		return url !== undefined;
	}

	this.getUrlAfterHas = function ()
	{
		return url;
	}

	this.setUrl = function (v)
	{
		url = v;
	}

	this.unsetUrl = function ()
	{
		url = undefined;
	}

}
