
const TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumbs = function (
	templateRenderer,
)
{

	this.render = function (
		breadcrumbs
	)
	{
		let widgetElement = this._renderTemplate();

		widgetElement = this._fillList(
			widgetElement,
			breadcrumbs
		);

		return widgetElement;
	}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-widget-breadcrumbs',
				{}
			);
		}

		this._fillList = function (widgetElement, breadcrumbs)
		{
			let olElement = widgetElement.querySelector('ol');

			for (let index = 0; index < breadcrumbs.length; index++)
			{
				let breadcrumb = breadcrumbs[index];
				let liElement = document.createElement('li');
				let breadcrumbElement = this._buildBreadcrumbElement(breadcrumb);
				liElement.appendChild(breadcrumbElement);
				olElement.appendChild(liElement);
			}

			return widgetElement;
		}

			this._buildBreadcrumbElement = function (breadcrumb)
			{
				let breadcrumbElement;

				if (breadcrumb.hasUrl())
				{
					breadcrumbElement = document.createElement('a');
					breadcrumbElement.setAttribute(
						'href',
						breadcrumb.getUrlAfterHas()
					);
				}
				else
				{
					breadcrumbElement = document.createElement('span');
				}

				let textNode = document.createTextNode(
					breadcrumb.getText()
				);
				breadcrumbElement.appendChild(textNode);

				return breadcrumbElement;
			}

}