
const TodoSpa_Book_Front_Widget_Footer_Footer = function (
	templateRenderer
)
{

	this.render = function ()
	{
		let widgetElement = this._renderTemplate();

		return widgetElement;
	}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-widget-footer',
				{
					appVersion: 'v' + TodoSpa_Constant.APP_VERSION
				}
			);
		}

}
