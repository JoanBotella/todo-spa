
const TodoSpa_Book_Front_Widget_Layout_Layout = function (
	templateRenderer,
	breadcrumbsWidget,
	slotFiller,
	homeRouteIdBuilder,
	footerWidget
)
{

	this.render = function (
		breadcrumbs,
		mainWidgetElement
	)
	{
		let widgetElement = this._renderTemplate();

		widgetElement = this._fillBreadcrumbs(
			widgetElement,
			breadcrumbs
		);
		
		widgetElement = this._fillMain(
			widgetElement,
			mainWidgetElement
			);

		widgetElement = this._fillFooter(widgetElement);

		return widgetElement;
	}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-widget-layout',
				{
					title: 'Todo SPA',
					homeRouteId: homeRouteIdBuilder.build(),
				}
			);
		}

		this._fillBreadcrumbs = function (widgetElement, breadcrumbs)
		{
			return slotFiller.fill(
				widgetElement,
				'breadcrumbs',
				this._renderBreadcrumbsWidget(breadcrumbs)
			);
		}

			this._renderBreadcrumbsWidget = function (breadcrumbs)
			{
				return breadcrumbsWidget.render(breadcrumbs);
			}

		this._fillMain = function (widgetElement, mainWidgetElement)
		{
			return slotFiller.fill(
				widgetElement,
				'main',
				mainWidgetElement
			);
		}

		this._fillFooter = function (widgetElement)
		{
			return slotFiller.fill(
				widgetElement,
				'footer',
				footerWidget.render()
			);
		}

}
