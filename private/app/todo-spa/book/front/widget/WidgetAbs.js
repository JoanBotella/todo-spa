
const TodoSpa_Book_Front_WidgetAbs = function (
	templateRenderer
)
{

	this._renderTemplate = function (templateName)
	{
		return templateRenderer.render(
			templateName,
			this._buildContext()
		);
	}

		this._buildContext = function ()
		{
			return {};
		}

}
