
const TodoSpa_Book_Front_Page_Home_Widget_Main_Main = function (
	templateRenderer,
	categoryListRouteIdBuilder,
	todoListRouteIdBuilder,
	dataMenuRouteIdBuilder,
	settingsShowRouteIdBuilder,
	aboutRouteIdBuilder,
)
{

	this.render = function ()
	{
		return this._renderTemplate();
	}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-page-home-widget-main',
				{
					title: 'Home',
					categoryListLink: 'Category List',
					categoryListRouteId: categoryListRouteIdBuilder.build(),
					todoListLink: 'Todo List',
					todoListRouteId: todoListRouteIdBuilder.build(),
					dataMenuLink: 'Data Menu',
					dataMenuRouteId: dataMenuRouteIdBuilder.build(),
					settingsShowLink: 'Settings Show',
					settingsShowRouteId: settingsShowRouteIdBuilder.build(),
					aboutLink: 'About',
					aboutRouteId: aboutRouteIdBuilder.build()
				}
			);
		}

}
