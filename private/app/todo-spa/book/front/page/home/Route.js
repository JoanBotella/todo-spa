
const TodoSpa_Book_Front_Page_Home_Route = function (
	serviceContainer
)
{

	this.getRouteIdMatcher = function ()
	{
		return serviceContainer.getBookFrontPageHomeRouteIdMatcher();
	}

	this.getController = function ()
	{
		return serviceContainer.getBookFrontPageHomeController();
	}

}