
const TodoSpa_Book_Front_Page_Todo_List_RouteIdMatcher = function (
)
{

	this.match = function (routeId)
	{
		return TodoSpa_Book_Front_Page_Todo_List_Constant.ROUTE_ID_REGEXP.test(routeId);
	}

}