
const TodoSpa_Book_Front_Page_Todo_List_Route = function (
	serviceContainer
)
{

	this.getRouteIdMatcher = function ()
	{
		return serviceContainer.getBookFrontPageTodoListRouteIdMatcher();
	}

	this.getController = function ()
	{
		return serviceContainer.getBookFrontPageTodoListController();
	}

}