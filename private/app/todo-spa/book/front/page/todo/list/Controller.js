
const TodoSpa_Book_Front_Page_Todo_List_Controller = function (
	routeArgsBuilder,
	slotFiller,
	layoutWidget,
	mainWidget,
	homeRouteIdBuilder,
)
{
	TodoSpa_Book_Front_ControllerAbs.call(
		this,
		routeArgsBuilder,
		slotFiller,
		layoutWidget
	);

	this._buildTitle = function ()
	{
		return 'Todo List';
	}

	this._buildBreadcrumbs = function ()
	{
		return [
			this._buildBreadcrumbHome(),
			this._buildBreadcrumbTodoList()
		];
	}

		this._buildBreadcrumbHome = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Home'
			);
			breadcrumb.setUrl(
				homeRouteIdBuilder.build()
			);
			return breadcrumb;
		}

		this._buildBreadcrumbTodoList = function ()
		{
			return new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Todo List'
			);
		}

	this._buildMainWidgetElement = function ()
	{
		return mainWidget.render();
	}

}
