
const TodoSpa_Book_Front_Page_Todo_List_Widget_Main_Main = function (
	templateRenderer,
	todoPersister,
	taskListRouteIdBuilder,
	todoAddRouteIdBuilder,
	todoDeleteUseCase,
	todoEditRouteIdBuilder,
)
{

	this.render = function ()
	{
		let widgetElement = this._renderTemplate();

		widgetElement = this._fillTodoList(widgetElement);

		return widgetElement;
	}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-page-todo-list-widget-main',
				{
					title: 'Todo List',
					todoAddRouteId: todoAddRouteIdBuilder.build(),
					addTodoLink: 'Add Todo'
				}
			);
		}

		this._fillTodoList = function (widgetElement)
		{
			let listElement = widgetElement.querySelector('[data-hook="todo_list"]');
			let todos = todoPersister.selectAll();

			for (let index = 0; index < todos.length; index++)
			{
				let itemElement = this._buildItemElement(todos[index]);
				listElement.appendChild(itemElement);
			}

			return widgetElement;
		}

			this._buildItemElement = function (todo)
			{
				let itemElement = document.createElement('li');
				itemElement.setAttribute(
					'data-id',
					todo.getId()
				);

				let nameLinkElement = this._buildNameLinkElement(todo);
				itemElement.appendChild(nameLinkElement);

				let actionsElement = this._buildActionsElement(todo);
				itemElement.appendChild(actionsElement);

				return itemElement;
			}

				this._buildNameLinkElement = function (todo)
				{
					let nameLinkElement = document.createElement('a');
					nameLinkElement.classList.add('name');

					let routeId = taskListRouteIdBuilder.build(todo.getSlug());
					nameLinkElement.setAttribute('href', routeId);
					nameLinkElement.setAttribute('title', 'Go to the task list page for the todo "' + todo.getName() + '".');
					let textNode = document.createTextNode(todo.getName());
					nameLinkElement.appendChild(textNode);

					return nameLinkElement;
				}

				this._buildActionsElement = function (todo)
				{
					let actionsElement = document.createElement('div');
					actionsElement.classList.add('actions');

					let deleteButtonElement = this._buildDeleteButtonElement(todo);
					actionsElement.appendChild(deleteButtonElement);
	
					let editLinkElement = this._buildEditLinkElement(todo);
					actionsElement.appendChild(editLinkElement);

					return actionsElement;
				}

					this._buildDeleteButtonElement = function (todo)
					{
						let deleteButtonElement = document.createElement('button');

						let textNode = document.createTextNode('Delete');
						deleteButtonElement.appendChild(textNode);

						deleteButtonElement.addEventListener(
							'click',
							function (event)
							{
								event.preventDefault();

								if (!confirm('Are you sure that you want to delete this todo? All its tasks will be deleted too.'))
								{
									return;
								}

								let liElement = event.target.closest('[data-id]');
								const id = Number(liElement.getAttribute('data-id'));

								todoDeleteUseCase.delete(
									id
								);

								liElement.parentElement.removeChild(
									liElement
								);
							}
						);

						return deleteButtonElement;
					}

					this._buildEditLinkElement = function (todo)
					{
						let editLinkElement = document.createElement('a');

						let routeId = todoEditRouteIdBuilder.build(todo.getSlug());
						editLinkElement.setAttribute('href', routeId);
						editLinkElement.setAttribute('title', 'Go to the todo edit page for the todo "' + todo.getName() + '".');
						let textNode = document.createTextNode('Edit');
						editLinkElement.appendChild(textNode);

						return editLinkElement;
					}

}
