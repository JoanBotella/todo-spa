
const TodoSpa_Book_Front_Page_Todo_Add_RouteIdMatcher = function (
)
{

	this.match = function (routeId)
	{
		return TodoSpa_Book_Front_Page_Todo_Add_Constant.ROUTE_ID_REGEXP.test(routeId);
	}

}