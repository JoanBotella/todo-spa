
const TodoSpa_Book_Front_Page_Todo_Add_Route = function (
	serviceContainer
)
{

	this.getRouteIdMatcher = function ()
	{
		return serviceContainer.getBookFrontPageTodoAddRouteIdMatcher();
	}

	this.getController = function ()
	{
		return serviceContainer.getBookFrontPageTodoAddController();
	}

}