
const TodoSpa_Book_Front_Page_Todo_Add_Widget_Main_Main = function (
	templateRenderer,
	todoAddUseCase,
	todoListRouteIdBuilder,
	router,
	slugSanitizer,
)
{

	this.render = function ()
	{
		let widgetElement = this._renderTemplate();

		widgetElement = this._fixTodoAddForm(widgetElement);

		return widgetElement;
	}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-page-todo-add-widget-main',
				{
					title: 'Todo Add',
					nameLabel: 'Name',
					slugLabel: 'Slug',
					submitButton: 'Save',
					todoListRouteId: todoListRouteIdBuilder.build()
				}
			);
		}

		this._fixTodoAddForm = function (widgetElement)
		{
			let formElement = widgetElement.querySelector('[data-hook="todo_add_form"]');

			formElement = this._addSlugAutogeneration(formElement);

			formElement = this._addSubmitEventListener(formElement);

			return widgetElement;
		}

			this._addSlugAutogeneration = function (formElement)
			{
				let nameElement = formElement.querySelector('[name="name"]');
				let slugElement = formElement.querySelector('[name="slug"]');

				nameElement.addEventListener(
					'input',
					function (event)
					{
						slugElement.value = slugSanitizer.sanitize(nameElement.value);
					}
				);

				return formElement;
			}

			this._addSubmitEventListener = function (formElement)
			{
				formElement.addEventListener(
					'submit',
					function (event)
					{
						event.preventDefault();
	
						const formData = new FormData(event.target);
	
						const todo = new TodoSpa_Persistence_Todo_Entity(
							0,
							formData.get('slug'),
							formData.get('name')
						);
	
						todoAddUseCase.add(todo);

						if (todoAddUseCase.hasError())
						{
							switch (todoAddUseCase.getErrorAfterHas())
							{
								case TodoSpa_UseCase_Todo_Constant.ERROR_SLUG_ALREADY_EXISTS:
									alert('The slug is already in use. Please choose another one.');
									break;
								default:
									alert('Something went wrong. The todo was not added.');
							}
							return;
						}

						router.route(
							todoListRouteIdBuilder.build()
						);
					}
				);
				return formElement;
			}

}
