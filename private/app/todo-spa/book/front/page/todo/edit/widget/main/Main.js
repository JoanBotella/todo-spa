
const TodoSpa_Book_Front_Page_Todo_Edit_Widget_Main_Main = function (
	templateRenderer,
	todoEditUseCase,
	todoListRouteIdBuilder,
	router,
	slugSanitizer,
)
{
	let todo;

	this.render = function (todoArg)
	{
		todo = todoArg;

		let widgetElement = this._renderTemplate();

		widgetElement = this._fixTodoEditForm(widgetElement);

		return widgetElement;
	}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-page-todo-edit-widget-main',
				{
					title: 'Todo Edit | Todo "' + todo.getName() + '"',
					nameLabel: 'Name',
					slugLabel: 'Slug',
					submitButton: 'Save',
					todoId: todo.getId(),
					todoSlug: todo.getSlug(),
					todoName: todo.getName()
				}
			);
		}

		this._fixTodoEditForm = function (widgetElement)
		{
			let formElement = widgetElement.querySelector('[data-hook="todo_edit_form"]');

			formElement = this._addSlugAutogeneration(formElement);

			formElement = this._addSubmitEventListener(formElement);

			return widgetElement;
		}

			this._addSlugAutogeneration = function (formElement)
			{
				let nameElement = formElement.querySelector('[name="name"]');
				let slugElement = formElement.querySelector('[name="slug"]');

				nameElement.addEventListener(
					'input',
					function (event)
					{
						slugElement.value = slugSanitizer.sanitize(nameElement.value);
					}
				);

				return formElement;
			}

			this._addSubmitEventListener = function (formElement)
			{
				formElement.addEventListener(
					'submit',
					function (event)
					{
						event.preventDefault();
	
						const formData = new FormData(event.target);
	
						const entity = new TodoSpa_Persistence_Todo_Entity(
							formData.get('id'),
							formData.get('slug'),
							formData.get('name')
						);
	
						todoEditUseCase.edit(
							entity.getId(),
							entity
						);

						if (todoEditUseCase.hasError())
						{
							switch (todoEditUseCase.getErrorAfterHas())
							{
								case TodoSpa_UseCase_Todo_Constant.ERROR_SLUG_ALREADY_EXISTS:
									alert('The slug is already in use. Please choose another one.');
									break;
								default:
									alert('Something went wrong. The todo was not edited.');
							}
							return;
						}

						router.route(
							todoListRouteIdBuilder.build()
						);
					}
				);
				return formElement;
			}

}
