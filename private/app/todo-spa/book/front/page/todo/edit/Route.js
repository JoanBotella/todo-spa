
const TodoSpa_Book_Front_Page_Todo_Edit_Route = function (
	serviceContainer
)
{

	this.getRouteIdMatcher = function ()
	{
		return serviceContainer.getBookFrontPageTodoEditRouteIdMatcher();
	}

	this.getController = function ()
	{
		return serviceContainer.getBookFrontPageTodoEditController();
	}

}