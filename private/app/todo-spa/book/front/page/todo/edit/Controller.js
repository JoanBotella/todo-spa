
const TodoSpa_Book_Front_Page_Todo_Edit_Controller = function (
	routeArgsBuilder,
	slotFiller,
	layoutWidget,
	mainWidget,
	homeRouteIdBuilder,
	todoListRouteIdBuilder,
	todoPersister,
)
{
	TodoSpa_Book_Front_ControllerAbs.call(
		this,
		routeArgsBuilder,
		slotFiller,
		layoutWidget
	);

	let todo;

	this._setup = function ()
	{
		todo = todoPersister.selectBySlug(
			this._getRouteArgs().getTodoSlug()
		)[0];
	}

	this._buildTitle = function ()
	{
		return 'Todo Edit | Todo "' + todo.getName() + '"';
	}

	this._buildBreadcrumbs = function ()
	{
		return [
			this._buildBreadcrumbHome(),
			this._buildBreadcrumbTodoList(),
			this._buildBreadcrumbTodoEdit()
		];
	}

		this._buildBreadcrumbHome = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Home'
			);
			breadcrumb.setUrl(
				homeRouteIdBuilder.build()
			);
			return breadcrumb;
		}

		this._buildBreadcrumbTodoList = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Todo List'
			);
			breadcrumb.setUrl(
				todoListRouteIdBuilder.build()
			);
			return breadcrumb;
		}

		this._buildBreadcrumbTodoEdit = function ()
		{
			return new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Todo Edit'
			);
		}

	this._buildMainWidgetElement = function ()
	{
		return mainWidget.render(todo);
	}

}
