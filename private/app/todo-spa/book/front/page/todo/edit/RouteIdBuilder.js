
const TodoSpa_Book_Front_Page_Todo_Edit_RouteIdBuilder = function ()
{

	this.build = function (todoSlug)
	{
		return 'todo/' + todoSlug + '/edit';
	}

}
