
const TodoSpa_Book_Front_Page_Todo_Edit_RouteArgs = function (
	todoSlug
)
{

	this.getTodoSlug = function ()
	{
		return todoSlug;
	}

	this.setTodoSlug = function (v)
	{
		todoSlug = v;
	}

}