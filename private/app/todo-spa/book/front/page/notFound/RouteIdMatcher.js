
const TodoSpa_Book_Front_Page_NotFound_RouteIdMatcher = function (
)
{

	this.match = function (routeId)
	{
		return TodoSpa_Book_Front_Page_NotFound_Constant.ROUTE_ID_REGEXP.test(routeId);
	}

}