
const TodoSpa_Book_Front_Page_NotFound_Route = function (
	serviceContainer
)
{

	this.getRouteIdMatcher = function ()
	{
		return serviceContainer.getBookFrontPageNotFoundRouteIdMatcher();
	}

	this.getController = function ()
	{
		return serviceContainer.getBookFrontPageNotFoundController();
	}

}