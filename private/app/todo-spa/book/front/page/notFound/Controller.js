
const TodoSpa_Book_Front_Page_NotFound_Controller = function (
	routeArgsBuilder,
	slotFiller,
	layoutWidget,
	mainWidget,
	homeRouteIdBuilder,
)
{
	TodoSpa_Book_Front_ControllerAbs.call(
		this,
		routeArgsBuilder,
		slotFiller,
		layoutWidget
	);

	this._buildTitle = function ()
	{
		return 'Not Found';
	}

	this._buildBreadcrumbs = function ()
	{
		return [
			this._buildBreadcrumbHome(),
			this._buildBreadcrumbNotFound()
		];
	}

		this._buildBreadcrumbHome = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Home'
			);
			breadcrumb.setUrl(
				homeRouteIdBuilder.build()
			);
			return breadcrumb;
		}

		this._buildBreadcrumbNotFound = function ()
		{
			return new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Not Found'
			);
		}

	this._buildMainWidgetElement = function ()
	{
		return mainWidget.render();
	}

}
