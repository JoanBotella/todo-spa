
const TodoSpa_Book_Front_Page_NotFound_Widget_Main_Main = function (
	templateRenderer,
)
{

	this.render = function ()
	{
		return this._renderTemplate();
	}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-page-not_found-widget-main',
				{
					title: 'Not Found'
				}
			);
		}

}
