
const TodoSpa_Book_Front_Page_Category_Edit_RouteIdBuilder = function ()
{

	this.build = function (categorySlug)
	{
		return 'category/' + categorySlug + '/edit';
	}

}
