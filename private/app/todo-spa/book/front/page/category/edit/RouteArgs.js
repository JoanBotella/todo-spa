
const TodoSpa_Book_Front_Page_Category_Edit_RouteArgs = function (
	categorySlug
)
{

	this.getCategorySlug = function ()
	{
		return categorySlug;
	}

	this.setCategorySlug = function (v)
	{
		categorySlug = v;
	}

}