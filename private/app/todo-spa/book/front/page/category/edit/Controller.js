
const TodoSpa_Book_Front_Page_Category_Edit_Controller = function (
	routeArgsBuilder,
	slotFiller,
	layoutWidget,
	mainWidget,
	homeRouteIdBuilder,
	categoryListRouteIdBuilder,
	categoryPersister,
)
{
	TodoSpa_Book_Front_ControllerAbs.call(
		this,
		routeArgsBuilder,
		slotFiller,
		layoutWidget
	);

	let category;

	this._setup = function ()
	{
		category = categoryPersister.selectBySlug(
			this._getRouteArgs().getCategorySlug()
		)[0];
	}

	this._buildTitle = function ()
	{
		return 'Category Edit | Category "' + category.getName() + '"';
	}

	this._buildBreadcrumbs = function ()
	{
		return [
			this._buildBreadcrumbHome(),
			this._buildBreadcrumbCategoryList(),
			this._buildBreadcrumbCategoryEdit()
		];
	}

		this._buildBreadcrumbHome = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Home'
			);
			breadcrumb.setUrl(
				homeRouteIdBuilder.build()
			);
			return breadcrumb;
		}

		this._buildBreadcrumbCategoryList = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Category List'
			);
			breadcrumb.setUrl(
				categoryListRouteIdBuilder.build()
			);
			return breadcrumb;
		}

		this._buildBreadcrumbCategoryEdit = function ()
		{
			return new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Category Edit'
			);
		}

	this._buildMainWidgetElement = function ()
	{
		return mainWidget.render(category);
	}

}
