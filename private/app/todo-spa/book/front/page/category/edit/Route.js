
const TodoSpa_Book_Front_Page_Category_Edit_Route = function (
	serviceContainer
)
{

	this.getRouteIdMatcher = function ()
	{
		return serviceContainer.getBookFrontPageCategoryEditRouteIdMatcher();
	}

	this.getController = function ()
	{
		return serviceContainer.getBookFrontPageCategoryEditController();
	}

}