
const TodoSpa_Book_Front_Page_Category_Edit_RouteIdMatcher = function (
)
{

	this.match = function (routeId)
	{
		return TodoSpa_Book_Front_Page_Category_Edit_Constant.ROUTE_ID_REGEXP.test(routeId);
	}

}