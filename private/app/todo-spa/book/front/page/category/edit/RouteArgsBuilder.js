
const TodoSpa_Book_Front_Page_Category_Edit_RouteArgsBuilder = function ()
{

	this.build = function (routeId)
	{
		let match = TodoSpa_Book_Front_Page_Category_Edit_Constant.ROUTE_ID_REGEXP.exec(routeId);
		return new TodoSpa_Book_Front_Page_Category_Edit_RouteArgs(match[1]);
	}

}