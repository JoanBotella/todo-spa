
const TodoSpa_Book_Front_Page_Category_Edit_Widget_Main_Main = function (
	templateRenderer,
	categoryEditUseCase,
	categoryListRouteIdBuilder,
	router,
	slugSanitizer,
)
{
	let category;

	this.render = function (categoryArg)
	{
		category = categoryArg;

		let widgetElement = this._renderTemplate();

		widgetElement = this._fixCategoryEditForm(widgetElement);

		return widgetElement;
	}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-page-category-edit-widget-main',
				{
					title: 'Category Edit | Category "' + category.getName() + '"',
					categoryId: category.getId(),
					categorySlug: category.getSlug(),
					categoryName: category.getName(),
					nameLabel: 'Name',
					slugLabel: 'Slug',
					submitButton: 'Save'
				}
			);
		}

		this._fixCategoryEditForm = function (widgetElement)
		{
			let formElement = widgetElement.querySelector('[data-hook="category_edit_form"]');

			formElement = this._addSlugAutogeneration(formElement);

			formElement = this._addSubmitEventListener(formElement);

			return widgetElement;
		}

			this._addSlugAutogeneration = function (formElement)
			{
				let nameElement = formElement.querySelector('[name="name"]');
				let slugElement = formElement.querySelector('[name="slug"]');

				nameElement.addEventListener(
					'input',
					function (event)
					{
						slugElement.value = slugSanitizer.sanitize(nameElement.value);
					}
				);

				return formElement;
			}

			this._addSubmitEventListener = function (formElement)
			{
				formElement.addEventListener(
					'submit',
					function (event)
					{
						event.preventDefault();

						const formData = new FormData(event.target);
	
						const entity = new TodoSpa_Persistence_Category_Entity(
							formData.get('id'),
							formData.get('slug'),
							formData.get('name')
						);

						categoryEditUseCase.edit(
							entity.getId(),
							entity
						);

						if (categoryEditUseCase.hasError())
						{
							switch (categoryEditUseCase.getErrorAfterHas())
							{
								case TodoSpa_UseCase_Category_Constant.ERROR_SLUG_ALREADY_EXISTS:
									alert('The slug is already in use. Please choose another one.');
									break;
								default:
									alert('Something went wrong. The category was not edited.');
							}
							return;
						}

						router.route(
							categoryListRouteIdBuilder.build()
						);
					}
				);
				return formElement;
			}

}
