
const TodoSpa_Book_Front_Page_Category_List_Widget_Main_Main = function (
	templateRenderer,
	categoryPersister,
	categoryAddRouteIdBuilder,
	categoryDeleteUseCase,
	categoryEditRouteIdBuilder,
)
{

	this.render = function ()
	{
		let widgetElement = this._renderTemplate();

		widgetElement = this._fillCategoryList(widgetElement);

		return widgetElement;
	}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-page-category-list-widget-main',
				{
					title: 'Category List',
					addCategoryLink: 'Add category',
					categoryAddRouteId: categoryAddRouteIdBuilder.build(),
				}
			);
		}

		this._fillCategoryList = function (widgetElement)
		{
			let listElement = widgetElement.querySelector('[data-hook="category_list"]');
			let categories = categoryPersister.selectAll();

			for (let index = 0; index < categories.length; index++)
			{
				let itemElement = this._buildItemElement(categories[index]);
				listElement.appendChild(itemElement);
			}

			return widgetElement;
		}

			this._buildItemElement = function (category)
			{
				let itemElement = document.createElement('li');
				itemElement.setAttribute(
					'data-id',
					category.getId()
				);

				let nameSpanElement = this._buildNameSpanElement(category);
				itemElement.appendChild(nameSpanElement);

				let actionsElement = this._buildActionsElement(category);
				itemElement.appendChild(actionsElement);

				return itemElement;
			}

				this._buildNameSpanElement = function (category)
				{
					let nameSpaElement = document.createElement('span');
					nameSpaElement.classList.add('name');

					let textNode = document.createTextNode(category.getName());
					nameSpaElement.appendChild(textNode);

					return nameSpaElement;
				}

				this._buildActionsElement = function (category)
				{
					let actionsElement = document.createElement('div');
					actionsElement.classList.add('actions');

					let deleteButtonElement = this._buildDeleteButtonElement(category);
					actionsElement.appendChild(deleteButtonElement);
	
					let editLinkElement = this._buildEditLinkElement(category);
					actionsElement.appendChild(editLinkElement);

					return actionsElement;
				}

					this._buildDeleteButtonElement = function (category)
					{
						let buttonElement = document.createElement('button');

						let textNode = document.createTextNode('Delete');
						buttonElement.appendChild(textNode);

						buttonElement.addEventListener(
							'click',
							function (event)
							{
								event.preventDefault();

								if (!confirm('Are you sure that you want to delete this category? All the tasks with this category will be uncategorized.'))
								{
									return;
								}

								let liElement = event.target.closest('[data-id]');
								const id = Number(liElement.getAttribute('data-id'));

								categoryDeleteUseCase.delete(
									id
								);

								liElement.parentElement.removeChild(
									liElement
								);
							}
						);

						return buttonElement;
					}

					this._buildEditLinkElement = function (category)
					{
						let editLinkElement = document.createElement('a');

						let routeId = categoryEditRouteIdBuilder.build(category.getSlug());
						editLinkElement.setAttribute('href', routeId);
						editLinkElement.setAttribute('title', 'Go to the category edit page for the category "' + category.getName() + '".');
						let textNode = document.createTextNode('Edit');
						editLinkElement.appendChild(textNode);

						return editLinkElement;
					}

}
