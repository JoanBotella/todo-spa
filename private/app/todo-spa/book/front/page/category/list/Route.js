
const TodoSpa_Book_Front_Page_Category_List_Route = function (
	serviceContainer
)
{

	this.getRouteIdMatcher = function ()
	{
		return serviceContainer.getBookFrontPageCategoryListRouteIdMatcher();
	}

	this.getController = function ()
	{
		return serviceContainer.getBookFrontPageCategoryListController();
	}

}