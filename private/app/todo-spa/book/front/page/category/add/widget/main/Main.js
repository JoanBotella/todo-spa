
const TodoSpa_Book_Front_Page_Category_Add_Widget_Main_Main = function (
	templateRenderer,
	categoryAddUseCase,
	categoryListRouteIdBuilder,
	router,
	slugSanitizer,
)
{

	this.render = function ()
	{
		let widgetElement = this._renderTemplate();

		widgetElement = this._fixCategoryAddForm(widgetElement);

		return widgetElement;
	}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-page-category-add-widget-main',
				{
					title: 'Category Add',
					nameLabel: 'Name',
					slugLabel: 'Slug'
				}
			);
		}

		this._fixCategoryAddForm = function (widgetElement)
		{
			let formElement = widgetElement.querySelector('[data-hook="category_add_form"]');

			formElement = this._addSlugAutogeneration(formElement);

			formElement = this._addSubmitEventListener(formElement);

			return widgetElement;
		}

			this._addSlugAutogeneration = function (formElement)
			{
				let nameElement = formElement.querySelector('[name="name"]');
				let slugElement = formElement.querySelector('[name="slug"]');

				nameElement.addEventListener(
					'input',
					function (event)
					{
						slugElement.value = slugSanitizer.sanitize(nameElement.value);
					}
				);

				return formElement;
			}

			this._addSubmitEventListener = function (formElement)
			{
				formElement.addEventListener(
					'submit',
					function (event)
					{
						event.preventDefault();
	
						const formData = new FormData(event.target);
	
						const category = new TodoSpa_Persistence_Category_Entity(
							0,
							formData.get('slug'),
							formData.get('name')
						);
	
						categoryAddUseCase.add(category);

						if (categoryAddUseCase.hasError())
						{
							switch (categoryAddUseCase.getErrorAfterHas())
							{
								case TodoSpa_UseCase_Category_Constant.ERROR_SLUG_ALREADY_EXISTS:
									alert('The slug is already in use. Please choose another one.');
									break;
								default:
									alert('Something went wrong. The category was not added.');
							}
							return;
						}

						router.route(
							categoryListRouteIdBuilder.build()
						);
					}
				);
				return formElement;
			}

}
