
const TodoSpa_Book_Front_Page_Category_Add_Route = function (
	serviceContainer
)
{

	this.getRouteIdMatcher = function ()
	{
		return serviceContainer.getBookFrontPageCategoryAddRouteIdMatcher();
	}

	this.getController = function ()
	{
		return serviceContainer.getBookFrontPageCategoryAddController();
	}

}