
const TodoSpa_Book_Front_Page_Task_Add_Controller = function (
	routeArgsBuilder,
	slotFiller,
	layoutWidget,
	mainWidget,
	homeRouteIdBuilder,
	todoListRouteIdBuilder,
	taskListRouteIdBuilder,
	todoPersister,
)
{
	TodoSpa_Book_Front_ControllerAbs.call(
		this,
		routeArgsBuilder,
		slotFiller,
		layoutWidget
	);

	let todo;

	this._setup = function ()
	{
		todo = todoPersister.selectBySlug(
			this._getRouteArgs().getTodoSlug()
		)[0];
	}

	this._buildTitle = function ()
	{
		return 'Task Add | Todo "' + todo.getName() + '"';
	}

	this._buildBreadcrumbs = function ()
	{
		return [
			this._buildBreadcrumbHome(),
			this._buildBreadcrumbTodoList(),
			this._buildBreadcrumbTaskList(),
			this._buildBreadcrumbTaskAdd(),
		];
	}

		this._buildBreadcrumbHome = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Home'
			);
			breadcrumb.setUrl(
				homeRouteIdBuilder.build()
			);
			return breadcrumb;
		}

		this._buildBreadcrumbTodoList = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Todo List'
			);
			breadcrumb.setUrl(
				todoListRouteIdBuilder.build()
			);
			return breadcrumb;
		}

		this._buildBreadcrumbTaskList = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Task List'
			);
			breadcrumb.setUrl(
				taskListRouteIdBuilder.build(
					todo.getSlug()
				)
			);
			return breadcrumb;
		}

		this._buildBreadcrumbTaskAdd = function ()
		{
			return new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Task Add'
			);
		}

	this._buildMainWidgetElement = function ()
	{
		return mainWidget.render(todo);
	}

}
