
const TodoSpa_Book_Front_Page_Task_Add_Route = function (
	serviceContainer
)
{

	this.getRouteIdMatcher = function ()
	{
		return serviceContainer.getBookFrontPageTaskAddRouteIdMatcher();
	}

	this.getController = function ()
	{
		return serviceContainer.getBookFrontPageTaskAddController();
	}

}