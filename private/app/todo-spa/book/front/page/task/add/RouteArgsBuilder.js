
const TodoSpa_Book_Front_Page_Task_Add_RouteArgsBuilder = function (
)
{

	this.build = function (routeId)
	{
		let match = TodoSpa_Book_Front_Page_Task_Add_Constant.ROUTE_ID_REGEXP.exec(routeId);
		return new TodoSpa_Book_Front_Page_Task_Add_RouteArgs(match[1]);
	}

}