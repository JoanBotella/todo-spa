
const TodoSpa_Book_Front_Page_Task_Add_Widget_Main_Main = function (
	templateRenderer,
	categoryPersister,
	taskAddUseCase,
	taskListRouteIdBuilder,
	router,
	slugSanitizer,
)
{

	const NO_CATEGORY_VALUE = '';

	let todo;

	this.render = function (todoArg)
	{
		todo = todoArg;

		let widgetElement = this._renderTemplate();

		widgetElement = this._fixTaskAddForm(widgetElement);

		return widgetElement;
	}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-page-task-add-widget-main',
				{
					title: 'Task Add | Todo "' + todo.getName() + '"',
					todoName: todo.getName(),
					nameLabel: 'Name',
					slugLabel: 'Slug',
					categoryLabel: 'Category',
					checkedLabel: 'Checked',
					submitButton: 'Save'
				}
			);
		}

		this._fixTaskAddForm = function (widgetElement)
		{
			let formElement = widgetElement.querySelector('[data-hook="task_add_form"]');

			formElement = this._addSlugAutogeneration(formElement);

			formElement = this._fixSelect(formElement);

			formElement = this._addSubmitEventListener(formElement);

			return widgetElement;
		}

			this._addSlugAutogeneration = function (formElement)
			{
				let nameElement = formElement.querySelector('[name="name"]');
				let slugElement = formElement.querySelector('[name="slug"]');

				nameElement.addEventListener(
					'input',
					function (event)
					{
						slugElement.value = slugSanitizer.sanitize(nameElement.value);
					}
				);

				return formElement;
			}

			this._fixSelect = function (formElement)
			{
				const categories = categoryPersister.selectAll();
				let selectElement = formElement.querySelector('select');

				selectElement = this._appendOptionNoCategory(selectElement);

				for (let index = 0; index < categories.length; index++)
				{
					selectElement = this._appendOptionByCategory(selectElement, categories[index]);
				}

				return formElement;
			}

				this._appendOptionNoCategory = function (selectElement)
				{
					const optionElement = document.createElement('option');

					optionElement.setAttribute('value', NO_CATEGORY_VALUE);

					let textNode = document.createTextNode('- None -');
					optionElement.appendChild(textNode);

					selectElement.appendChild(optionElement);

					return selectElement;
				}

				this._appendOptionByCategory = function (selectElement, category)
				{
					const optionElement = document.createElement('option');

					optionElement.setAttribute('value', category.getId());

					let textNode = document.createTextNode(category.getName());
					optionElement.appendChild(textNode);

					selectElement.appendChild(optionElement);

					return selectElement;
				}

			this._addSubmitEventListener = function(formElement)
			{
				formElement.addEventListener(
					'submit',
					function (event)
					{
						event.preventDefault();
	
						const formData = new FormData(event.target);
	
						const task = new TodoSpa_Persistence_Task_Entity(
							0,
							formData.get('slug'),
							todo.getId(),
							formData.get('name'),
							formData.get('checked') !== null
						);
	
						const categoryId = formData.get('category-id');
	
						if (categoryId !== NO_CATEGORY_VALUE)
						{
							task.setCategoryId(categoryId);
						}
	
						taskAddUseCase.add(task);
	
						if (taskAddUseCase.hasError())
						{
							switch (taskAddUseCase.getErrorAfterHas())
							{
								case TodoSpa_UseCase_Task_Constant.ERROR_SLUG_AND_TODOID_ALREADY_EXIST:
									alert('The slug is already in use. Please choose another one.');
									break;
								default:
									alert('Something went wrong. The task was not added.');
							}
							return;
						}

						router.route(
							taskListRouteIdBuilder.build(
								todo.getSlug()
							)
						);
					}
				);
				return formElement;
			}

}
