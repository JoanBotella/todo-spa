
const TodoSpa_Book_Front_Page_Task_Add_RouteIdBuilder = function ()
{

	this.build = function (todoSlug)
	{
		return 'todo/' + todoSlug + '/task/add';
	}

}
