
const TodoSpa_Book_Front_Page_Task_List_Widget_Main_Main = function (
	templateRenderer,
	taskPersister,
	categoryPersister,
	taskAddRouteIdBuilder,
	taskDeleteUseCase,
	taskEditRouteIdBuilder,
	taskCheckUseCase,
	tasksSorter,
)
{
	let todo;

	this.render = function (todoArg)
	{
		todo = todoArg;

		let widgetElement = this._renderTemplate();

		widgetElement = this._fixLists(widgetElement);

		return widgetElement;
	}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-page-task-list-widget-main',
				{
					title: 'Task List | Todo "' + todo.getName() + '"',
					addTaskLink: 'Add Task',
					'todoName': todo.getName(),
					taskAddRouteId: taskAddRouteIdBuilder.build(todo.getSlug()),
				}
			);
		}

		this._fixLists = function (widgetElement)
		{
			let lists = this._buildLists();

			let listsContainerElement = widgetElement.querySelector('[data-hook="task_lists_box"]');

			for (let categoryId in lists)
			{
				let categoryListCardElement = this._buildCategoryListCardElement();

				let categoryTitleElement = this._buildCategoryTitle(categoryId);
				categoryListCardElement.appendChild(categoryTitleElement);

				let categoryListElement = this._buildCategoryList(lists[categoryId]);
				categoryListCardElement.appendChild(categoryListElement);

				listsContainerElement.appendChild(categoryListCardElement);
			}

			return widgetElement;
		}

			this._buildLists = function ()
			{
				let tasks = taskPersister.selectByTodoId(
					todo.getId()
				);
				tasks = tasksSorter.sort(tasks);
				let lists = {};

				for (let index = 0; index < tasks.length; index++)
				{
					let task = tasks[index];
					let categoryId =
						task.hasCategoryId()
							? task.getCategoryIdAfterHas()
							: ''
					;

					if (lists[categoryId] === undefined)
					{
						lists[categoryId] = [];
					}

					lists[categoryId].push(task);
				}

				return lists;
			}

			this._buildCategoryListCardElement = function ()
			{
				let categoryListCardElement = document.createElement('div');
				categoryListCardElement.classList.add('card');
				return categoryListCardElement;
			}

			this._buildCategoryTitle = function (categoryId)
			{
				let titleElement = document.createElement('h3');

				let categoryName =
					categoryId == ''
						? 'Sense Categoria'
						: categoryPersister.selectById(categoryId)[0].getName()
				;

				let textNode = document.createTextNode(categoryName);
				titleElement.appendChild(textNode);

				return titleElement;
			}

			this._buildCategoryList = function (list)
			{
				let categoryListElement = document.createElement('ul');

				for (let index = 0; index < list.length; index++)
				{
					let categoryListItemElement = this._buildCategoryListItem(list[index]);
					categoryListElement.appendChild(categoryListItemElement);
				}

				return categoryListElement;
			}

				this._buildCategoryListItem = function (task)
				{
					let categoryListItemElement = document.createElement('li');
					categoryListItemElement.setAttribute(
						'data-id',
						task.getId()
					);

					let nameSpanElement = this._buildNameSpanElement(task);
					categoryListItemElement.appendChild(nameSpanElement);
	
					let actionsElement = this._buildActionsElement(task);
					categoryListItemElement.appendChild(actionsElement);

					return categoryListItemElement;
				}

					this._buildNameSpanElement = function (task)
					{
						let nameSpanElement = document.createElement('span');
						nameSpanElement.classList.add('name');

						nameSpanElement.setAttribute(
							'data-checked',
							task.getChecked()
								? 'checked'
								: 'unchecked'
						);

						let textNode = document.createTextNode(task.getName());
						nameSpanElement.appendChild(textNode);

						nameSpanElement.addEventListener(
							'click',
							function (event)
							{
								const target = event.target;

								const checked = target.getAttribute('data-checked') == 'checked';

								let liElement = target.closest('[data-id]');
								let ulElement = liElement.parentElement;
								const id = Number(liElement.getAttribute('data-id'));

								taskCheckUseCase.check(
									id,
									!checked
								);

								let nextCheckedAttributeValue =
									checked
										? 'unchecked'
										: 'checked'
								;

								target.setAttribute(
									'data-checked',
									nextCheckedAttributeValue
								);

								let liElementName = liElement.querySelector('[data-checked]').textContent;

								if (checked)
								{
									// go up

									let pointer = liElement.previousElementSibling;

									if (pointer !== null)
									{
										let pointerSpanElement = pointer.querySelector('[data-checked]');
										liElement.remove();
										while (
											pointer !== null
											&& (
												pointerSpanElement.getAttribute('data-checked') == 'checked'
												|| pointerSpanElement.textContent > liElementName
											)
										)
										{
											pointer = pointer.previousElementSibling;
											if (pointer !== null)
											{
												pointerSpanElement = pointer.querySelector('[data-checked]');
											}
										}
										if (pointer === null)
										{
											ulElement.prepend(liElement);
										}
										else
										{
											pointer.after(liElement);
										}
									}
								}
								else
								{
									// go down

									let pointer = liElement.nextElementSibling;
									
									if (pointer !== null)
									{
										let pointerSpanElement = pointer.querySelector('[data-checked]');
										liElement.remove();
										while (
											pointer !== null
											&& (
												pointerSpanElement.getAttribute('data-checked') == 'unchecked'
												|| pointerSpanElement.textContent < liElementName
											)
										)
										{
											pointer = pointer.nextElementSibling;
											if (pointer !== null)
											{
												pointerSpanElement = pointer.querySelector('[data-checked]');
											}
										}
										if (pointer === null)
										{
											ulElement.append(liElement);
										}
										else
										{
											pointer.before(liElement);
										}
									}
								}
							}
						);

						return nameSpanElement;
					}

					this._buildActionsElement = function (task)
					{
						let actionsElement = document.createElement('div');
						actionsElement.classList.add('actions');

						let deleteButtonElement = this._buildDeleteButtonElement(task);
						actionsElement.appendChild(deleteButtonElement);
		
						let editLinkElement = this._buildEditLinkElement(task);
						actionsElement.appendChild(editLinkElement);

						return actionsElement;
					}

						this._buildDeleteButtonElement = function (task)
						{
							let deleteButtonElement = document.createElement('button');

							let textNode = document.createTextNode('Delete');
							deleteButtonElement.appendChild(textNode);

							deleteButtonElement.addEventListener(
								'click',
								function (event)
								{
									event.preventDefault();

									if (!confirm('Are you sure that you want to delete this task?'))
									{
										return;
									}
	
									const target = event.target;
									const liElement = target.closest('[data-id]');
									const ulElement = liElement.parentElement;
									const id = Number(liElement.getAttribute('data-id'));

									taskDeleteUseCase.delete(
										id
									);

									ulElement.removeChild(liElement);

									if (ulElement.children.length == 0)
									{
										let cardElement = ulElement.parentElement;
										cardElement.parentElement.removeChild(
											cardElement
										);
									}
								}
							);

							return deleteButtonElement;
						}

						this._buildEditLinkElement = function (task)
						{
							let editLinkElement = document.createElement('a');

							let routeId = taskEditRouteIdBuilder.build(
								task.getSlug(),
								todo.getSlug()
							);
							editLinkElement.setAttribute('href', routeId);
							editLinkElement.setAttribute('title', 'Go to the task edit page for the task "' + task.getName() + '".');
							let textNode = document.createTextNode('Edit');
							editLinkElement.appendChild(textNode);

							return editLinkElement;
						}

}
