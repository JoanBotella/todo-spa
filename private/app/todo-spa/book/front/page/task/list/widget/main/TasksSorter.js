
const TodoSpa_Book_Front_Page_Task_List_Widget_Main_TasksSorter = function (
	categoryPersister
)
{

	let _categoryIdsToNamesMap;

	this.sort = function (tasks)
	{
		this._setup();
		const r = this._run(tasks);
		this._tearDown();
		return r;
	}

		this._setup = function ()
		{
			this._categoryIdsToNamesMap = {};
		}

		this._tearDown = function ()
		{
			this._categoryIdsToNamesMap = null;
		}

		this._run = function (tasks)
		{
			const wrappedTasks = this._buildWrappedTasks(tasks);
			const sortedWrappedTasks = this._sortWrappedTasks(wrappedTasks);
			return this._unwrapTasks(sortedWrappedTasks);
		}

			this._buildWrappedTasks = function (tasks)
			{
				let wrappedTasks = [];

				for (let index = 0; index < tasks.length; index++)
				{
					let task = this._wrapTask(tasks[index]);
					wrappedTasks.push(task);
				}

				return wrappedTasks;
			}

				this._wrapTask = function (task)
				{
					return {
						meta: {
							categoryName: this._getCategoryName(task),
							checked: task.getChecked(),
							name: task.getName(),
						},
						task: task
					};
				}

					this._getCategoryName = function (task)
					{
						if (!task.hasCategoryId())
						{
							return '';
						}

						const categoryId = task.getCategoryIdAfterHas();

						if (categoryId in this._categoryIdsToNamesMap)
						{
							return this._categoryIdsToNamesMap[categoryId];
						}

						const result = categoryPersister.selectById(categoryId);

						if (result.length == 0)
						{
							throw new Error('Category id "' + categoryId + '" not found.');
						}

						const categoryName = result[0].getName();

						this._categoryIdsToNamesMap[categoryId] = categoryName;

						return categoryName;
					}

			this._sortWrappedTasks = function (wrappedTasks)
			{
				wrappedTasks.sort(
					this._compareWrappedTasks
				);
				return wrappedTasks;
			}

				this._compareWrappedTasks = function (a, b)
				{
					const aMeta = a.meta;
					const bMeta = b.meta;

					if (aMeta.categoryName > bMeta.categoryName)
					{
						return 1;
					}
					if (aMeta.categoryName < bMeta.categoryName)
					{
						return -1;
					}

					if (aMeta.checked && !bMeta.checked)
					{
						return 1;
					}
					if (!aMeta.checked && bMeta.checked)
					{
						return -1;
					}

					if (aMeta.name > bMeta.name)
					{
						return 1;
					}
					if (aMeta.name < bMeta.name)
					{
						return -1;
					}
					return 0;
				}

			this._unwrapTasks = function (wrappedTasks)
			{
				let tasks = [];

				for (let index = 0; index < wrappedTasks.length; index++)
				{
					let task = wrappedTasks[index].task;
					tasks.push(task);
				}

				return tasks;
			}

}