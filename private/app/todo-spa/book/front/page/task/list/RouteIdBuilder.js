
const TodoSpa_Book_Front_Page_Task_List_RouteIdBuilder = function ()
{

	this.build = function (todoSlug)
	{
		return 'todo/' + todoSlug + '/task/list';
	}

}
