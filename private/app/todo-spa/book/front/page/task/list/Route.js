
const TodoSpa_Book_Front_Page_Task_List_Route = function (
	serviceContainer
)
{

	this.getRouteIdMatcher = function ()
	{
		return serviceContainer.getBookFrontPageTaskListRouteIdMatcher();
	}

	this.getController = function ()
	{
		return serviceContainer.getBookFrontPageTaskListController();
	}

}