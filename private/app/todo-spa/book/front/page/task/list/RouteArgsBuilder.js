
const TodoSpa_Book_Front_Page_Task_List_RouteArgsBuilder = function ()
{

	this.build = function (routeId)
	{
		let match = TodoSpa_Book_Front_Page_Task_List_Constant.ROUTE_ID_REGEXP.exec(routeId);
		return new TodoSpa_Book_Front_Page_Task_List_RouteArgs(match[1]);
	}

}