
const TodoSpa_Book_Front_Page_Task_Edit_RouteArgs = function (
	todoSlug,
	taskSlug
)
{

	this.getTodoSlug = function ()
	{
		return todoSlug;
	}

	this.setTodoSlug = function (v)
	{
		todoSlug = v;
	}

	this.getTaskSlug = function ()
	{
		return taskSlug;
	}

	this.setTaskSlug = function (v)
	{
		taskSlug = v;
	}

}