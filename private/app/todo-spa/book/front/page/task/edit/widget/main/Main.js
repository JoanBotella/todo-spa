
const TodoSpa_Book_Front_Page_Task_Edit_Widget_Main_Main = function (
	templateRenderer,
	todoPersister,
	categoryPersister,
	taskEditUseCase,
	taskListRouteIdBuilder,
	router,
	slugSanitizer,
)
{

	const NO_CATEGORY_VALUE = '';

	let todo;
	let task;

	this.render = function (taskArg)
	{
		task = taskArg;
		this._setupTodo();

		let widgetElement = this._renderTemplate();

		widgetElement = this._fixTaskEditForm(widgetElement);

		return widgetElement;
	}

		this._setupTodo = function ()
		{
			todo = todoPersister.selectById(
				task.getTodoId()
			)[0];
		}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-page-task-edit-widget-main',
				{
					title: 'Task Edit | Task "' + task.getName() + '" | Todo "' + todo.getName() + '"',
					taskId: task.getId(),
					taskName: task.getName(),
					taskSlug: task.getSlug(),
					nameLabel: 'Name',
					slugLabel: 'Slug',
					categoryLabel: 'Category',
					checkedLabel: 'Checked',
					submitButton: 'Save'
				}
			);
		}

		this._fixTaskEditForm = function (widgetElement)
		{
			let formElement = widgetElement.querySelector('[data-hook="task_edit_form"]');

			formElement = this._addSlugAutogeneration(formElement);

			formElement = this._fixCheckbox(formElement);

			formElement = this._fixSelect(formElement);

			formElement = this._addSubmitEventListener(formElement);

			return widgetElement;
		}

			this._addSlugAutogeneration = function (formElement)
			{
				let nameElement = formElement.querySelector('[name="name"]');
				let slugElement = formElement.querySelector('[name="slug"]');

				nameElement.addEventListener(
					'input',
					function (event)
					{
						slugElement.value = slugSanitizer.sanitize(nameElement.value);
					}
				);

				return formElement;
			}

			this._fixCheckbox = function (formElement)
			{
				if (task.getChecked())
				{
					const checkboxElement = formElement.querySelector('[type="checkbox"]');
					checkboxElement.setAttribute('checked', '');
				}
				return formElement;
			}

			this._fixSelect = function (formElement)
			{
				const categories = categoryPersister.selectAll();
				let selectElement = formElement.querySelector('select');

				selectElement = this._appendOptionNoCategory(selectElement);

				for (let index = 0; index < categories.length; index++)
				{
					selectElement = this._appendOptionByCategory(selectElement, categories[index]);
				}

				return formElement;
			}

				this._appendOptionNoCategory = function (selectElement)
				{
					const optionElement = document.createElement('option');

					optionElement.setAttribute('value', NO_CATEGORY_VALUE);

					if (!task.hasCategoryId())
					{
						optionElement.setAttribute('selected', '');
					}

					let textNode = document.createTextNode('- None -');
					optionElement.appendChild(textNode);

					selectElement.appendChild(optionElement);

					return selectElement;
				}

				this._appendOptionByCategory = function (selectElement, category)
				{
					const optionElement = document.createElement('option');
					const categoryId = category.getId();

					optionElement.setAttribute('value', categoryId);

					if (
						task.hasCategoryId()
						&& task.getCategoryIdAfterHas() == categoryId
					)
					{
						optionElement.setAttribute('selected', '');
					}

					let textNode = document.createTextNode(category.getName());
					optionElement.appendChild(textNode);

					selectElement.appendChild(optionElement);

					return selectElement;
				}

			this._addSubmitEventListener = function (formElement)
			{
				formElement.addEventListener(
					'submit',
					function (event)
					{
						event.preventDefault();
	
						const formData = new FormData(event.target);
	
						const entity = new TodoSpa_Persistence_Task_Entity(
							formData.get('id'),
							formData.get('slug'),
							todo.getId(),
							formData.get('name'),
							formData.get('checked') !== null
						);
	
						const categoryId = formData.get('category-id');
	
						if (categoryId != NO_CATEGORY_VALUE)
						{
							entity.setCategoryId(categoryId);
						}
	
						taskEditUseCase.edit(
							entity.getId(),
							entity
						);

						if (taskEditUseCase.hasError())
						{
							switch (taskEditUseCase.getErrorAfterHas())
							{
								case TodoSpa_UseCase_Task_Constant.ERROR_SLUG_AND_TODOID_ALREADY_EXIST:
									alert('The slug is already in use. Please choose another one.');
									break;
								default:
									alert('Something went wrong. The task was not edited.');
							}
							return;
						}

						router.route(
							taskListRouteIdBuilder.build(
								todo.getSlug()
							)
						);
					}
				);
				return formElement;
			}

}
