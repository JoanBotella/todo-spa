
const TodoSpa_Book_Front_Page_Task_Edit_Controller = function (
	routeArgsBuilder,
	slotFiller,
	layoutWidget,
	mainWidget,
	homeRouteIdBuilder,
	todoListRouteIdBuilder,
	taskListRouteIdBuilder,
	todoPersister,
	taskPersister,
)
{
	TodoSpa_Book_Front_ControllerAbs.call(
		this,
		routeArgsBuilder,
		slotFiller,
		layoutWidget
	);

	let todo;
	let task;

	this._setup = function ()
	{
		const routeArgs = this._getRouteArgs();

		todo = todoPersister.selectBySlug(
			routeArgs.getTodoSlug()
		)[0];

		task = taskPersister.selectBySlugAndTodoId(
			routeArgs.getTaskSlug(),
			todo.getId()
		)[0];
	}

	this._buildTitle = function ()
	{
		return 'Task Edit | Task "' + task.getName() + '" | Todo "' + todo.getName() + '"';
	}

	this._buildBreadcrumbs = function ()
	{
		return [
			this._buildBreadcrumbHome(),
			this._buildBreadcrumbTodoList(),
			this._buildBreadcrumbTaskList(),
			this._buildBreadcrumbTaskEdit(),
		];
	}

		this._buildBreadcrumbHome = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Home'
			);
			breadcrumb.setUrl(
				homeRouteIdBuilder.build()
			);
			return breadcrumb;
		}

		this._buildBreadcrumbTodoList = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Todo List'
			);
			breadcrumb.setUrl(
				todoListRouteIdBuilder.build()
			);
			return breadcrumb;
		}

		this._buildBreadcrumbTaskList = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Task List'
			);
			breadcrumb.setUrl(
				taskListRouteIdBuilder.build(
					todo.getSlug()
				)
			);
			return breadcrumb;
		}

		this._buildBreadcrumbTaskEdit = function ()
		{
			return new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Task Edit'
			);
		}

	this._buildMainWidgetElement = function ()
	{
		return mainWidget.render(task);
	}

}
