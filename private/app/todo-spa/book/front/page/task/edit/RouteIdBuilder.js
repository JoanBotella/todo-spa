
const TodoSpa_Book_Front_Page_Task_Edit_RouteIdBuilder = function ()
{

	this.build = function (taskSlug, todoSlug)
	{
		return 'todo/' + todoSlug + '/task/' + taskSlug + '/edit';
	}

}
