
const TodoSpa_Book_Front_Page_About_Route = function (
	serviceContainer
)
{

	this.getRouteIdMatcher = function ()
	{
		return serviceContainer.getBookFrontPageAboutRouteIdMatcher();
	}

	this.getController = function ()
	{
		return serviceContainer.getBookFrontPageAboutController();
	}

}