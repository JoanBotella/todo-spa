
const TodoSpa_Book_Front_Page_About_Widget_Main_Main = function (
	templateRenderer,
)
{

	this.render = function ()
	{
		return this._renderTemplate();
	}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-page-about-widget-main',
				{
					title: 'About'
				}
			);
		}

}
