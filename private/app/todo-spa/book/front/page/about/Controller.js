
const TodoSpa_Book_Front_Page_About_Controller = function (
	routeArgsBuilder,
	slotFiller,
	layoutWidget,
	mainWidget,
	homeRouteIdBuilder,
)
{
	TodoSpa_Book_Front_ControllerAbs.call(
		this,
		routeArgsBuilder,
		slotFiller,
		layoutWidget
	);

	this._buildTitle = function ()
	{
		return 'About';
	}

	this._buildBreadcrumbs = function ()
	{
		return [
			this._buildBreadcrumbHome(),
			this._buildBreadcrumbAbout()
		];
	}

		this._buildBreadcrumbHome = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Home'
			);
			breadcrumb.setUrl(
				homeRouteIdBuilder.build()
			);
			return breadcrumb;
		}

		this._buildBreadcrumbAbout = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'About'
			);
			return breadcrumb;
		}

	this._buildMainWidgetElement = function ()
	{
		return mainWidget.render();
	}

}
