
const TodoSpa_Book_Front_Page_Data_Edit_Route = function (
	serviceContainer
)
{

	this.getRouteIdMatcher = function ()
	{
		return serviceContainer.getBookFrontPageDataEditRouteIdMatcher();
	}

	this.getController = function ()
	{
		return serviceContainer.getBookFrontPageDataEditController();
	}

}