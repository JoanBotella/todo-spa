
const TodoSpa_Book_Front_Page_Data_Edit_Widget_Main_Main = function (
	templateRenderer,
	homeRouteIdBuilder,
	router,
	storageContainer,
	databaseMigrator
)
{

	this.render = function ()
	{
		let widgetElement = this._renderTemplate();

		widgetElement = this._fixDataEditForm(widgetElement);

		return widgetElement;
	}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-page-data-edit-widget-main',
				{
					title: 'Data Edit',
					dataLabel: 'Data',
					submitButton: 'Save',
				}
			);
		}

		this._fixDataEditForm = function (widgetElement)
		{
			let formElement = widgetElement.querySelector('[data-hook="data_edit_form"]');

			formElement = this._fillDataTextarea(formElement);

			formElement = this._addBeautifyButtonClickEventListener(formElement);

			formElement = this._addSubmitEventListener(formElement);

			return widgetElement;
		}

			this._fillDataTextarea = function (formElement)
			{
				let dataElement = formElement.querySelector('[name="data"]');

				dataElement.value = JSON.stringify(
					storageContainer.get()
				);

				return formElement;
			}

			this._addBeautifyButtonClickEventListener = function (formElement)
			{
				let beautifyButtonElement = formElement.querySelector('[data-hook="beautify"]');

				beautifyButtonElement.addEventListener(
					'click',
					function (event)
					{
						event.preventDefault();

						let dataElement = formElement.querySelector('[name="data"]');
						let json;
						
						try
						{
							json = JSON.parse(dataElement.value);
						}
						catch (exception)
						{
							alert('Invalid JSON.');
							return;
						}

						dataElement.value = JSON.stringify(json, null, "\t");
					}
				);

				return formElement;
			}

			this._addSubmitEventListener = function (formElement)
			{
				formElement.addEventListener(
					'submit',
					function (event)
					{
						event.preventDefault();

						const formData = new FormData(event.target);
						let newDb;

						try
						{
							newDb = JSON.parse(
								formData.get('data')
							);
						}
						catch (exception)
						{
							alert('Invalid JSON. Data was not updated.');
							return;
						}

						storageContainer.set(newDb);

						databaseMigrator.migrateIfRequired();

						router.route(
							homeRouteIdBuilder.build()
						);
					}
				);
				return formElement;
			}

}
