
const TodoSpa_Book_Front_Page_Data_Download_Widget_Main_Main = function (
	templateRenderer,
	metaPersister,
	settingsPersister,
	storageContainer,
	databaseMigrator
)
{
	let
		firstPhaseBoxElement,
		localTbodyElement,
		checkRemoteDataButtonElement,
		secondPhaseBoxElement,
		remoteTbodyElement,
		downloadRemoteDataButtonElement
	;

	let
		meta,
		settings,
		remoteData
	;

	this.render = function ()
	{
		let widgetElement = this._renderTemplate();

		widgetElement = this._fixInterface(widgetElement);

		return widgetElement;
	}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-page-data-download-widget-main',
				{
					title: 'Data Download',
					checkRemoteDataButton: 'Check remote data',
					downloadRemoteDataButton: 'Download remote data',
				}
			);
		}

		this._fixInterface = function (widgetElement)
		{
			this._setupInterfaceElements(widgetElement);
			this._setupSettings();
			this._setupMeta();

			this._hideElement(secondPhaseBoxElement);

			if (!this._areSettingsConfigured())
			{
				this._hideElement(firstPhaseBoxElement);
				alert('You need to configure all settings before using this feature.');
				return widgetElement;
			}

			this._fixFirstPhaseInterface();

			return widgetElement;
		}

			this._setupInterfaceElements = function (widgetElement)
			{
				firstPhaseBoxElement = widgetElement.querySelector('[data-hook="first_phase_box"]');
				localTbodyElement = widgetElement.querySelector('[data-hook="local_tbody"]');
				checkRemoteDataButtonElement = widgetElement.querySelector('[data-hook="check_remote_data_button"]');
				secondPhaseBoxElement = widgetElement.querySelector('[data-hook="second_phase_box"]');
				remoteTbodyElement = widgetElement.querySelector('[data-hook="remote_tbody"]');
				downloadRemoteDataButtonElement = widgetElement.querySelector('[data-hook="download_remote_data_button"]');
			}

			this._setupSettings = function ()
			{
				settings = settingsPersister.selectById(0)[0];
			}

			this._setupMeta = function ()
			{
				meta = metaPersister.selectById(0)[0];
			}

			this._areSettingsConfigured = function ()
			{
				return (
					settings.hasUserName()
					&& settings.hasDeviceName()
					&& settings.hasServerUrl()
				);
			}

			this._hideElement = function (element)
			{
				element.style.display = 'none';
			}

			this._fixFirstPhaseInterface = function ()
			{
				this._fillLocalTbody();
				this._addClickEventListenerToCheckRemoteDataButton();
			}

				this._fillLocalTbody = function ()
				{
					this._fillTbody(
						localTbodyElement,
						{
							'Database Version': meta.getVersion(),
							'Database Last change': new Date(meta.getLastChange()).toUTCString(),
							'User name': settings.getUserNameAfterHas(),
							'Device name': settings.getDeviceNameAfterHas(),
							'Server URL': settings.getServerUrlAfterHas(),
						}
					);
				}

					this._fillTbody = function (tbodyElement, items)
					{
						for (let key in items)
						{
							tbodyElement.appendChild(
								this._buildTrElement(key, items[key])
							);
						}
					}

					this._buildTrElement = function (key, value)
					{
						let trElement = document.createElement('tr');

						let keyElement = document.createElement('th');
						keyElement.appendChild(
							document.createTextNode(key)
						);
						trElement.appendChild(keyElement);

						let valueElement = document.createElement('td');
						valueElement.appendChild(
							document.createTextNode(value)
						);
						trElement.appendChild(valueElement);

						return trElement;
					}

				this._addClickEventListenerToCheckRemoteDataButton = function ()
				{
					let that = this;
					checkRemoteDataButtonElement.addEventListener(
						'click',
						function (event)
						{
							event.preventDefault();
							that._fetchRemoteData();
						}
					);
				}

					this._fetchRemoteData = function ()
					{
						remoteData = {};

						const request = new Request(
							settings.getServerUrlAfterHas()
						);
						fetch(request)
							.then(
								(response) =>
								{
									switch (response.status)
									{
										case 404:
											alert('No data was found on the server.');
											break;
										case 200:
											response.json()
												.then(
													(json) =>
													{
														remoteData = json;
														this._fixSeconPhaseInterface();
													}
												)
												.catch(
													(error) =>
													{
														alert('The server content is bad formatted.');
													}
												);
											break;
										default:
											alert('Something went wrong. Maybe the server is not compatible. Response status: ' + response.status + '.');
									}
								}
							)
							.catch(
								(error) =>
								{
									alert('Something went wrong. Is the server URL correct?');
								}
							)
						;
					}

					this._fixSeconPhaseInterface = function ()
					{
						let wasVisible = this._areSecondPhaseElementsVisible();

						if (wasVisible)
						{
							this._emptyRemoteTbody();
						}
						this._fillRemoteTbody();

						if (!wasVisible)
						{
							this._showElement(secondPhaseBoxElement);
							this._addClickEventListenerToDownloadRemoteDataButton();
						}
					}

						this._areSecondPhaseElementsVisible = function ()
						{
							return secondPhaseBoxElement.style.display == 'block';
						}

						this._showElement = function (element)
						{
							element.style.display = 'block';
						}

						this._emptyRemoteTbody = function ()
						{
							remoteTbodyElement.innerHTML = '';
						}

						this._fillRemoteTbody = function ()
						{
							this._fillTbody(
								remoteTbodyElement,
								{
									'Database Version': this._getRemoteDbVersion(),
									'Database Last change': this._getRemoteDbLastChange(),
									'User name': this._getRemoteDbUserName(),
									'Device name': this._getRemoteDbDeviceName(),
									'Server URL': this._getRemoteDbServerUrl(),
								}
							);
						}

							this._getRemoteDbVersion = function ()
							{
								let value;
								try
								{
									value = remoteData.meta[0].version;
								}
								catch (exception)
								{
									value = '?';
								}
								return value;
							}

							this._getRemoteDbLastChange = function ()
							{
								let value;
								try
								{
									value = new Date(remoteData.meta[0].lastChange).toUTCString();
								}
								catch (exception)
								{
									value = '?';
								}
								return value;
							}

							this._getRemoteDbUserName = function ()
							{
								let value;
								try
								{
									value = remoteData.settings[0].userName;
								}
								catch (exception)
								{
									value = '?';
								}
								return value;
							}

							this._getRemoteDbDeviceName = function ()
							{
								let value;
								try
								{
									value = remoteData.settings[0].deviceName;
								}
								catch (exception)
								{
									value = '?';
								}
								return value;
							}

							this._getRemoteDbServerUrl = function ()
							{
								let value;
								try
								{
									value = remoteData.settings[0].serverUrl;
								}
								catch (exception)
								{
									value = '?';
								}
								return value;
							}

						this._addClickEventListenerToDownloadRemoteDataButton = function ()
						{
							let that = this;
							downloadRemoteDataButtonElement.addEventListener(
								'click',
								function (event)
								{
									event.preventDefault();
									that._downloadRemoteData();
								}
							);
						}

							this._downloadRemoteData = function ()
							{
								let localData = storageContainer.get();
								remoteData.settings = localData.settings;
								storageContainer.set(remoteData);

								databaseMigrator.migrateIfRequired();
								alert('Data downloaded!');
							}

}
