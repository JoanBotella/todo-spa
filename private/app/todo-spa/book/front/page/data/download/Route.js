
const TodoSpa_Book_Front_Page_Data_Download_Route = function (
	serviceContainer
)
{

	this.getRouteIdMatcher = function ()
	{
		return serviceContainer.getBookFrontPageDataDownloadRouteIdMatcher();
	}

	this.getController = function ()
	{
		return serviceContainer.getBookFrontPageDataDownloadController();
	}

}