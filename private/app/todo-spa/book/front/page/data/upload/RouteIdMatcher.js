
const TodoSpa_Book_Front_Page_Data_Upload_RouteIdMatcher = function (
)
{

	this.match = function (routeId)
	{
		return TodoSpa_Book_Front_Page_Data_Upload_Constant.ROUTE_ID_REGEXP.test(routeId);
	}

}