
const TodoSpa_Book_Front_Page_Data_Upload_Route = function (
	serviceContainer
)
{

	this.getRouteIdMatcher = function ()
	{
		return serviceContainer.getBookFrontPageDataUploadRouteIdMatcher();
	}

	this.getController = function ()
	{
		return serviceContainer.getBookFrontPageDataUploadController();
	}

}