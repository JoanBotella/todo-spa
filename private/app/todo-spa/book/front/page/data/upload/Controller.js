
const TodoSpa_Book_Front_Page_Data_Upload_Controller = function (
	routeArgsBuilder,
	slotFiller,
	layoutWidget,
	mainWidget,
	homeRouteIdBuilder,
	dataMenuRouteIdBuilder,
)
{
	TodoSpa_Book_Front_ControllerAbs.call(
		this,
		routeArgsBuilder,
		slotFiller,
		layoutWidget
	);

	this._buildTitle = function ()
	{
		return 'Data Upload';
	}

	this._buildBreadcrumbs = function ()
	{
		return [
			this._buildBreadcrumbHome(),
			this._buildBreadcrumbDataMenu(),
			this._buildBreadcrumbDataUpload(),
		];
	}

		this._buildBreadcrumbHome = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Home'
			);
			breadcrumb.setUrl(
				homeRouteIdBuilder.build()
			);
			return breadcrumb;
		}

		this._buildBreadcrumbDataMenu = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Data Menu'
			);
			breadcrumb.setUrl(
				dataMenuRouteIdBuilder.build()
			);
			return breadcrumb;
		}

		this._buildBreadcrumbDataUpload = function ()
		{
			return new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Data Upload'
			);
		}

	this._buildMainWidgetElement = function ()
	{
		return mainWidget.render();
	}

}
