
const TodoSpa_Book_Front_Page_Data_Menu_RouteIdMatcher = function (
)
{

	this.match = function (routeId)
	{
		return TodoSpa_Book_Front_Page_Data_Menu_Constant.ROUTE_ID_REGEXP.test(routeId);
	}

}