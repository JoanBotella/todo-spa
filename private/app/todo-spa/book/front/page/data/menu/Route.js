
const TodoSpa_Book_Front_Page_Data_Menu_Route = function (
	serviceContainer
)
{

	this.getRouteIdMatcher = function ()
	{
		return serviceContainer.getBookFrontPageDataMenuRouteIdMatcher();
	}

	this.getController = function ()
	{
		return serviceContainer.getBookFrontPageDataMenuController();
	}

}