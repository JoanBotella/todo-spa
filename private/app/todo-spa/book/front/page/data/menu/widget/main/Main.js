
const TodoSpa_Book_Front_Page_Data_Menu_Widget_Main_Main = function (
	templateRenderer,
	dataDownloadRouteIdBuilder,
	dataUploadRouteIdBuilder,
	dataEditRouteIdBuilder,
)
{

	this.render = function ()
	{
		let widgetElement = this._renderTemplate();

		return widgetElement;
	}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-page-data-menu-widget-main',
				{
					title: 'Data Menu',
					dataDownloadRouteId: dataDownloadRouteIdBuilder.build(),
					dataDownloadLink: 'Data Download',
					dataUploadRouteId: dataUploadRouteIdBuilder.build(),
					dataUploadLink: 'Data Upload',
					dataEditRouteId: dataEditRouteIdBuilder.build(),
					dataEditLink: 'Data Edit',
				}
			);
		}

}
