
const TodoSpa_Book_Front_Page_Settings_Edit_Controller = function (
	routeArgsBuilder,
	slotFiller,
	layoutWidget,
	mainWidget,
	homeRouteIdBuilder,
	settingsShowRouteIdBuilder,
)
{
	TodoSpa_Book_Front_ControllerAbs.call(
		this,
		routeArgsBuilder,
		slotFiller,
		layoutWidget
	);

	this._buildTitle = function ()
	{
		return 'Settings Edit';
	}

	this._buildBreadcrumbs = function ()
	{
		return [
			this._buildBreadcrumbHome(),
			this._buildBreadcrumbSettingsShow(),
			this._buildBreadcrumbSettingsEdit(),
		];
	}

		this._buildBreadcrumbHome = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Home'
			);
			breadcrumb.setUrl(
				homeRouteIdBuilder.build()
			);
			return breadcrumb;
		}

		this._buildBreadcrumbSettingsShow = function ()
		{
			const breadcrumb = new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Settings Show'
			);
			breadcrumb.setUrl(
				settingsShowRouteIdBuilder.build()
			);
			return breadcrumb;
		}

		this._buildBreadcrumbSettingsEdit = function ()
		{
			return new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumb(
				'Settings Edit'
			);
		}

	this._buildMainWidgetElement = function ()
	{
		return mainWidget.render();
	}

}
