
const TodoSpa_Book_Front_Page_Settings_Edit_Widget_Main_Main = function (
	templateRenderer,
	settingsPersister,
	settingsEditUseCase,
	settingsShowRouteIdBuilder,
	router,
)
{
	let settings;

	this.render = function ()
	{
		this._setupSettings();

		let widgetElement = this._renderTemplate();

		widgetElement = this._fixSettingsEditForm(widgetElement);

		return widgetElement;
	}

		this._setupSettings = function ()
		{
			settings = settingsPersister.selectAll()[0];
		}

		this._renderTemplate = function ()
		{
			return templateRenderer.render(
				'todo_spa-book-front-page-settings-edit-widget-main',
				{
					title: 'Settings Edit',
					id: settings.getId(),
					userNameLabel: 'User name',
					userName: settings.hasUserName() ? settings.getUserNameAfterHas() : '',
					deviceNameLabel: 'Device name',
					deviceName: settings.hasDeviceName() ? settings.getDeviceNameAfterHas() : '',
					serverUrlLabel: 'Server URL',
					serverUrl: settings.hasServerUrl() ? settings.getServerUrlAfterHas() : '',
					submitButton: 'Save',
				}
			);
		}

		this._fixSettingsEditForm = function (widgetElement)
		{
			let formElement = widgetElement.querySelector('[data-hook="settings_edit_form"]');

			formElement = this._addSubmitEventListener(formElement);

			return widgetElement;
		}

			this._addSubmitEventListener = function (formElement)
			{
				formElement.addEventListener(
					'submit',
					function (event)
					{
						event.preventDefault();

						const formData = new FormData(event.target);

						const entity = new TodoSpa_Persistence_Settings_Entity(
							formData.get('id'),
						);
						entity.setUserName(
							formData.get('user_name')
						);
						entity.setDeviceName(
							formData.get('device_name')
						);
						entity.setServerUrl(
							formData.get('server_url')
						);

						settingsEditUseCase.edit(
							entity.getId(),
							entity,
						);

						router.route(
							settingsShowRouteIdBuilder.build()
						);
					}
				);
				return formElement;
			}

}
