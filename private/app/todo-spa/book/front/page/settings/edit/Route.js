
const TodoSpa_Book_Front_Page_Settings_Edit_Route = function (
	serviceContainer
)
{

	this.getRouteIdMatcher = function ()
	{
		return serviceContainer.getBookFrontPageSettingsEditRouteIdMatcher();
	}

	this.getController = function ()
	{
		return serviceContainer.getBookFrontPageSettingsEditController();
	}

}