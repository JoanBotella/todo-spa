
const TodoSpa_Book_Front_Page_Settings_Show_Route = function (
	serviceContainer
)
{

	this.getRouteIdMatcher = function ()
	{
		return serviceContainer.getBookFrontPageSettingsShowRouteIdMatcher();
	}

	this.getController = function ()
	{
		return serviceContainer.getBookFrontPageSettingsShowController();
	}

}