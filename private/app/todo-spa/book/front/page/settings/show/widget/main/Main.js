
const TodoSpa_Book_Front_Page_Settings_Show_Widget_Main_Main = function (
	templateRenderer,
	settingsPersister,
	settingsEditRouteIdBuilder,
)
{

	this.render = function ()
	{
		return this._renderTemplate();
	}

		this._renderTemplate = function ()
		{
			const settings = this._getSettings();
			return templateRenderer.render(
				'todo_spa-book-front-page-settings-show-widget-main',
				{
					title: 'Settings Show',
					settingsEditRouteId: settingsEditRouteIdBuilder.build(),
					settingsEditLink: 'Edit settings',
					userNameLabel: 'User name',
					userName: settings.hasUserName() ? settings.getUserNameAfterHas() : '',
					deviceNameLabel: 'Device name',
					deviceName: settings.hasDeviceName() ? settings.getDeviceNameAfterHas() : '',
					serverUrlLabel: 'Server URL',
					serverUrl: settings.hasServerUrl() ? settings.getServerUrlAfterHas() : '',
				}
			);
		}

			this._getSettings = function ()
			{
				return settingsPersister.selectAll()[0];
			}

}
