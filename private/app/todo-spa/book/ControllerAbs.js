

const TodoSpa_Book_ControllerAbs = function (
	routeArgsBuilder,
	slotFiller
)
{
	let routeId;

	this._getRouteId = function ()
	{
		return routeId;
	}

	let routeArgs;

	this._getRouteArgs = function ()
	{
		return routeArgs;
	}

	this.run = function (ri)
	{
		routeId = ri;
		routeArgs = this._buildRouteArgs();

		this._setup();

		this._render();
	}

		this._buildRouteArgs = function ()
		{
			return routeArgsBuilder.build(
				this._getRouteId()
			);
		}

		this._setup = function ()
		{
		}

		this._render = function ()
		{
			document.title = this._buildTitle() + ' | Todo SPA';

			this._fillLayout();
		}

			this._buildTitle = function ()
			{
				throw new Error('Not implemented');
			}

			this._fillLayout = function ()
			{
				slotFiller.fill(
					document,
					'layout',
					this._buildLayoutWidgetElement()
				);
			}

				this._buildLayoutWidgetElement = function ()
				{
					throw new Error('Not implemented');
				}

}
