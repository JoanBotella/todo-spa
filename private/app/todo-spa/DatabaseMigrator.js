
const TodoSpa_DatabaseMigrator = function (
	storageContainer,
	metaPersister
)
{

	this.migrateIfRequired = function ()
	{
		let dbVersion = this._getDbVersion();

		if (dbVersion == 0)
		{
			this._singleObject();
			this._addIds();
			this._addMetaTable();
			dbVersion++;
		}

		if (dbVersion == 1)
		{
			this._turnTaskSlugsIntoIds();
			dbVersion++;
		}

		this._updateDbVersion(dbVersion);
	}

		this._getDbVersion = function ()
		{
			if (
				localStorage.getItem('todo') !== null
				|| localStorage.getItem('todoSpa') === null
				|| JSON.parse(localStorage.getItem('todoSpa'))['meta'] === undefined
			)
			{
				return 0;
			}
			return metaPersister.selectById(0)[0].getVersion();
		}

		this._singleObject = function ()
		{
			if (localStorage.getItem('todoSpa') !== null)
			{
				localStorage.removeItem('todo');
				localStorage.removeItem('task');
				localStorage.removeItem('category');
				localStorage.removeItem('settings');
				return;
			}

			let db = {};
			let parsed;

			if (localStorage.getItem('todo') !== null)
			{
				parsed = JSON.parse(localStorage.getItem('todo'));
				if (parsed !== null)
				{
					db['todo'] = parsed;
					localStorage.removeItem('todo');
				}
			}

			if (localStorage.getItem('task') !== null)
			{
				parsed = JSON.parse(localStorage.getItem('task'));
				if (parsed !== null)
				{
					db['task'] = parsed;
					localStorage.removeItem('task');
				}
			}

			if (localStorage.getItem('category') !== null)
			{
				parsed = JSON.parse(localStorage.getItem('category'));
				if (parsed !== null)
				{
					db['category'] = parsed;
					localStorage.removeItem('category');
				}
			}

			if (localStorage.getItem('settings') !== null)
			{
				parsed = JSON.parse(localStorage.getItem('settings'));
				if (parsed !== null)
				{
					db['settings'] = parsed;
					localStorage.removeItem('settings');
				}
			}

			storageContainer.set(db);
		}

		this._addIds = function ()
		{
			let db = storageContainer.get();

			let tables = [
				'settings',
				'category',
				'todo',
				'task',
			];

			for (let tableIndex = 0; tableIndex < tables.length; tableIndex++)
			{
				let table = tables[tableIndex];
				if (db[table] === undefined)
				{
					continue;
				}

				let id = 0;

				for (let elementIndex = 0; elementIndex < db[table].length; elementIndex++)
				{
					let element = db[table][elementIndex];

					if (element.id === undefined)
					{
						element.id = id;
						id++;
						continue;
					}

					if (typeof element.id == 'string')
					{
						element.id = Number(element.id);
					}
				}
			}

			storageContainer.set(db);
		}

		this._addMetaTable = function ()
		{
			let db = storageContainer.get();

			db['meta'] = [
				{
					id: 0,
					version: 1,
					lastChange: Date.now()
				}
			];

			storageContainer.set(db);
		}

		this._turnTaskSlugsIntoIds = function ()
		{
			let
				db = storageContainer.get(),
				tasks = db.task
			;

			if (tasks === undefined)
			{
				return;
			}

			let
				todoSlugToId = this._buildTodoSlugToId(db),
				categorySlugToId = this._buildCategorySlugToId(db),
				task
			;
			for (let index = 0; index < tasks.length; index++)
			{
				task = tasks[index];

				task.slug = task.taskSlug;
				delete(task.taskSlug);

				task.todoId = todoSlugToId[task.todoSlug];
				delete(task.todoSlug);

				task.categoryId = categorySlugToId[task.categorySlug];
				delete(task.categorySlug);
			}
			db.task = tasks;

			storageContainer.set(db);
		}

			this._buildTodoSlugToId = function (db)
			{
				let
					todos = db.todo,
					todoSlugToId = {}
				;

				if (todos === undefined)
				{
					return todoSlugToId;
				}

				let todo;
				for (let index = 0; index < todos.length; index++)
				{
					todo = todos[index];
					todoSlugToId[todo.slug] = todo.id;
				}

				return todoSlugToId;
			}

			this._buildCategorySlugToId = function (db)
			{
				let
					categories = db.category,
					categorySlugToId = {}
				;

				if (categories === undefined)
				{
					return categorySlugToId;
				}

				let category;
				for (let index = 0; index < categories.length; index++)
				{
					category = categories[index];
					categorySlugToId[category.slug] = category.id;
				}

				return categorySlugToId;
			}

		this._updateDbVersion = function (dbVersion)
		{
			let db = storageContainer.get();
			db.meta[0].version = dbVersion;
			storageContainer.set(db);
		}

}
