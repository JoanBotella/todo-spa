
const TodoSpa_UseCase_Todo_Edit = function (
	todoPersister
)
{
	let error = null;

	this.edit = function (id, todo)
	{
		this._unsetError();

		this._checkForErrorSlugAlreadyExists(id, todo);
		if (this.hasError())
		{
			return;
		}

		todoPersister.updateById(id, todo);
	}

		this._checkForErrorSlugAlreadyExists = function (id, newTodo)
		{
			let oldTodo = todoPersister.selectById(id)[0];

			if (oldTodo.getSlug() == newTodo.getSlug())
			{
				return;
			}

			let todos = todoPersister.selectBySlug(
				newTodo.getSlug()
			);

			if (todos.length > 0)
			{
				this._setError(
					TodoSpa_UseCase_Todo_Constant.ERROR_SLUG_ALREADY_EXISTS
				);
			}
		}

	this.hasError = function ()
	{
		return error !== null;
	}

	this.getErrorAfterHas = function ()
	{
		return error;
	}

	this._setError = function (v)
	{
		error = v;
	}

	this._unsetError = function ()
	{
		error = null;
	}

}
