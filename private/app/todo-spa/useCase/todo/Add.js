
const TodoSpa_UseCase_Todo_Add = function (
	todoPersister
)
{
	let error = null;

	this.add = function (todo)
	{
		this._unsetError();

		this._checkForErrorSlugAlreadyExists(todo);
		if (this.hasError())
		{
			return;
		}

		todoPersister.insert(todo);
	}

		this._checkForErrorSlugAlreadyExists = function (todo)
		{
			let todos = todoPersister.selectBySlug(
				todo.getSlug()
			);
			if (todos.length > 0)
			{
				this._setError(
					TodoSpa_UseCase_Todo_Constant.ERROR_SLUG_ALREADY_EXISTS
				);
			}
		}

	this.hasError = function ()
	{
		return error !== null;
	}

	this.getErrorAfterHas = function ()
	{
		return error;
	}

	this._setError = function (v)
	{
		error = v;
	}

	this._unsetError = function ()
	{
		error = null;
	}

}
