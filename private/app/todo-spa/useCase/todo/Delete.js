
const TodoSpa_UseCase_Todo_Delete = function (
	todoPersister,
	taskPersister
)
{

	this.delete = function(id)
	{
		todoPersister.deleteById(id);
		taskPersister.deleteByTodoId(id);
	}

}
