
const TodoSpa_UseCase_Category_Add = function (
	categoryPersister
)
{
	let error = null;

	this.add = function (category)
	{
		this._unsetError();

		this._checkForErrorSlugAlreadyExists(category);
		if (this.hasError())
		{
			return;
		}

		categoryPersister.insert(category);
	}

		this._checkForErrorSlugAlreadyExists = function (category)
		{
			let categories = categoryPersister.selectBySlug(
				category.getSlug()
			);
			if (categories.length > 0)
			{
				this._setError(
					TodoSpa_UseCase_Category_Constant.ERROR_SLUG_ALREADY_EXISTS
				);
			}
		}

	this.hasError = function ()
	{
		return error !== null;
	}

	this.getErrorAfterHas = function ()
	{
		return error;
	}

	this._setError = function (v)
	{
		error = v;
	}

	this._unsetError = function ()
	{
		error = null;
	}

}
