
const TodoSpa_UseCase_Category_Edit = function (
	categoryPersister
)
{
	let error = null;

	this.edit = function (id, category)
	{
		this._unsetError();

		this._checkForErrorSlugAlreadyExists(id, category);
		if (this.hasError())
		{
			return;
		}

		categoryPersister.updateById(id, category);
	}

		this._checkForErrorSlugAlreadyExists = function (id, newCategory)
		{
			let oldCategory = categoryPersister.selectById(id)[0];

			if (oldCategory.getSlug() == newCategory.getSlug())
			{
				return;
			}

			let categories = categoryPersister.selectBySlug(
				newCategory.getSlug()
			);

			if (categories.length > 0)
			{
				this._setError(
					TodoSpa_UseCase_Category_Constant.ERROR_SLUG_ALREADY_EXISTS
				);
			}
		}

	this.hasError = function ()
	{
		return error !== null;
	}

	this.getErrorAfterHas = function ()
	{
		return error;
	}

	this._setError = function (v)
	{
		error = v;
	}

	this._unsetError = function ()
	{
		error = null;
	}

}
