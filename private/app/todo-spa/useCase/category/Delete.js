
const TodoSpa_UseCase_Category_Delete = function (
	categoryPersister,
	taskPersister
)
{

	this.delete = function(id)
	{
		categoryPersister.deleteById(id);
		taskPersister.unsetCategoryId(id);
	}

}
