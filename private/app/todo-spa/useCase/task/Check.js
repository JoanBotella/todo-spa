
const TodoSpa_UseCase_Task_Check = function (
	taskPersister
)
{

	this.check = function (id, checked)
	{
		taskPersister.updateChecked(
			id,
			checked
		);
	}

}
