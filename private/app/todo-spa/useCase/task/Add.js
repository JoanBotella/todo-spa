
const TodoSpa_UseCase_Task_Add = function (
	taskPersister
)
{
	let error = null;

	this.add = function (task)
	{
		this._unsetError();

		this._checkForErrorSlugAndTodoIdAlreadyExist(task);
		if (this.hasError())
		{
			return;
		}

		taskPersister.insert(task);
	}

		this._checkForErrorSlugAndTodoIdAlreadyExist = function (task)
		{
			let tasks = taskPersister.selectBySlugAndTodoId(
				task.getSlug(),
				task.getTodoId()
			);
	
			if (tasks.length > 0)
			{
				this._setError(
					TodoSpa_UseCase_Task_Constant.ERROR_SLUG_AND_TODOID_ALREADY_EXIST
				);
			}
		}

	this.hasError = function ()
	{
		return error !== null;
	}

	this.getErrorAfterHas = function ()
	{
		return error;
	}

	this._setError = function (v)
	{
		error = v;
	}

	this._unsetError = function ()
	{
		error = null;
	}

}
