
const TodoSpa_UseCase_Task_Delete = function (
	taskPersister
)
{

	this.delete = function (id)
	{
		taskPersister.deleteById(id);
	}

}
