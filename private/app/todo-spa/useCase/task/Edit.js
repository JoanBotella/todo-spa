
const TodoSpa_UseCase_Task_Edit = function (
	taskPersister
)
{
	let error = null;

	this.edit = function (id, task)
	{
		this._unsetError();

		this._checkForErrorTaskSlugAndTodoSlugAlreadyExist(id, task);
		if (this.hasError())
		{
			return;
		}

		taskPersister.updateById(id, task);
	}

		this._checkForErrorTaskSlugAndTodoSlugAlreadyExist = function (id, newTask)
		{
			let oldTask = taskPersister.selectById(id)[0];

			if (
				oldTask.getSlug() == newTask.getSlug()
				&& oldTask.getTodoId() == newTask.getTodoId()
			)
			{
				return;
			}

			let tasks = taskPersister.selectBySlugAndTodoId(
				newTask.getSlug(),
				newTask.getTodoId()
			);

			if (tasks.length > 0)
			{
				this._setError(
					TodoSpa_UseCase_Task_Constant.ERROR_SLUG_AND_TODOID_ALREADY_EXIST
				);
			}
		}

	this.hasError = function ()
	{
		return error !== null;
	}

	this.getErrorAfterHas = function ()
	{
		return error;
	}

	this._setError = function (v)
	{
		error = v;
	}

	this._unsetError = function ()
	{
		error = null;
	}

}
