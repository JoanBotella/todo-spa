
const TodoSpa_UseCase_Settings_Edit = function (
	settingsPersister
)
{

	this.edit = function (id, settings)
	{
		settingsPersister.updateById(id, settings);
	}

}
