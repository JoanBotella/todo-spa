
const TodoSpa_Persistence_PersisterAbs = function (
	storageContainer
)
{
	let nextId = null;

	this.selectAll = function ()
	{
		return this._filteredSelect(
			function (row)
			{
				return true;
			},
			0
		);
	}

		this._filteredSelect = function (filterCallback, limit)
		{
			const table = this._getTable();
			let result = [];

			for (let index = 0; index < table.length; index++)
			{
				let row = table[index];

				if (filterCallback(row))
				{
					result.push(
						this._buildEntityByRow(row)
					);

					if (result.length == limit)
					{
						break;
					}
				}
			}

			return result;
		}

		this._getTable = function ()
		{
			const
				db = storageContainer.get(),
				tableName = this._getTableName()
			;

			if (db[tableName] === undefined)
			{
				db[tableName] = this._buildDefaultTable();
				storageContainer.set(db);
			}

			return db[tableName];
		}

			this._getTableName = function ()
			{
				throw new Error('Not implemented');
			}

			this._buildDefaultTable = function ()
			{
				return [];
			}

		this._buildEntityByRow = function (row)
		{
			throw new Error('Not implemented');
		}

	this.selectById = function (id)
	{
		return this._filteredSelect(
			function (row)
			{
				return row.id == id;
			},
			1
		);
	}

	this.insert = function (entity)
	{
		let
			table = this._getTable(),
			row = this._buildRowByEntity(entity)
		;

		row.id = this._getNextIdAndIncrementIt();

		table.push(row);

		this._setTable(table);
	}

		this._getNextIdAndIncrementIt = function ()
		{
			if (nextId === null)
			{
				nextId = this._buildNextId();
			}
			let r = nextId;
			nextId++;
			return r;
		}

			this._buildNextId = function ()
			{
				let
					table = this._getTable(),
					maxId = -1
				;

				for (let index = 0; index < table.length; index++)
				{
					let row = table[index];
					if (row.id > maxId)
					{
						maxId = row.id;
					}
				}

				return maxId+1;
			}

		this._buildRowByEntity = function (entity)
		{
			throw new Error('Not implemented');
		}

		this._setTable = function (table)
		{
			let db = storageContainer.get();
			const tableName = this._getTableName();
			db[tableName] = table;
			storageContainer.set(db);
		}

	this.deleteById = function (id)
	{
		this._filteredDelete(
			function (row)
			{
				return row.id == id;
			},
			1
		);
	}

		this._filteredDelete = function (filterCallback, limit)
		{
			let table = this._getTable();
			let affectedRows = 0;

			for (let index = 0; index < table.length; index++)
			{
				let row = table[index];

				if (filterCallback(row))
				{
					table.splice(index, 1);
					affectedRows++;

					if (affectedRows == limit)
					{
						break;
					}

					index--;
				}
			}

			if (affectedRows > 0)
			{
				this._setTable(table);
			}

			return affectedRows;
		}

	this.updateById = function (id, entity)
	{
		let that = this;
		return this._filteredUpdate(
			function (row)
			{
				return row.id == id;
			},
			function (row)
			{
				return that._buildRowByEntity(entity);
			},
			1
		);
	}

		this._filteredUpdate = function (filterCallback, updateCallback, limit)
		{
			let table = this._getTable();
			let affectedRows = 0;
	
			for (let index = 0; index < table.length; index++)
			{
				let row = table[index];
	
				if (filterCallback(row))
				{
					table[index] = updateCallback(row);
					affectedRows++;
	
					if (affectedRows == limit)
					{
						break;
					}
				}
			}
	
			if (affectedRows > 0)
			{
				this._setTable(table);
			}
	
			return affectedRows;
		}

}
