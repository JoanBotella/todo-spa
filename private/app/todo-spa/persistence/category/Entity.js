
const TodoSpa_Persistence_Category_Entity = function (
	id,
	slug,
	name
)
{
	TodoSpa_Persistence_EntityAbs.call(
		this,
		id
	);

	this.getSlug = function ()
	{
		return slug;
	}

	this.setSlug = function (v)
	{
		slug = v;
	}

	this.getName = function ()
	{
		return name;
	}

	this.setName = function (v)
	{
		name = v;
	}

}
