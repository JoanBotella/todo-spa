
const TodoSpa_Persistence_Todo_Persister = function (
	storageContainer
)
{
	TodoSpa_Persistence_PersisterAbs.call(
		this,
		storageContainer
	);

	this._getTableName = function ()
	{
		return 'todo';
	}

	this._buildEntityByRow = function (row)
	{
		return new TodoSpa_Persistence_Todo_Entity(
			row.id,
			row.slug,
			row.name
		);
	}

	this._buildRowByEntity = function (entity)
	{
		return {
			id: entity.getId(),
			slug: entity.getSlug(),
			name: entity.getName()
		};
	}

	this.selectBySlug = function (slug)
	{
		return this._filteredSelect(
			function (row)
			{
				return row.slug == slug;
			},
			1
		);
	}

}