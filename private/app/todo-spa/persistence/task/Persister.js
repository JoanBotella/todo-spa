
const TodoSpa_Persistence_Task_Persister = function (
	storageContainer,
	categoryPersister,
	todoPersister
)
{
	TodoSpa_Persistence_PersisterAbs.call(
		this,
		storageContainer
	);

	this._getTableName = function ()
	{
		return 'task';
	}

	this._buildEntityByRow = function (row)
	{
		let entity = new TodoSpa_Persistence_Task_Entity(
			row.id,
			row.slug,
			row.todoId,
			row.name,
			row.checked
		);

		if (row.categoryId !== null)
		{
			entity.setCategoryId(row.categoryId);
		}

		return entity;
	}

	this._buildRowByEntity = function (entity)
	{
		return {
			id: entity.getId(),
			slug: entity.getSlug(),
			todoId: entity.getTodoId(),
			name: entity.getName(),
			checked: entity.getChecked(),
			categoryId: entity.hasCategoryId() ? entity.getCategoryIdAfterHas() : null
		};
	}

	this.selectByTodoId = function (todoId)
	{
		return this._filteredSelect(
			function (row)
			{
				return row.todoId == todoId;
			},
			0
		);
	}

	this.selectBySlugAndTodoId = function (slug, todoId)
	{
		return this._filteredSelect(
			function (row)
			{
				return (
					row.slug == slug
					&& row.todoId == todoId
				);
			},
			0
		);
	}

	this.deleteByTodoId = function (todoId)
	{
		return this._filteredDelete(
			function (row)
			{
				return row.todoId == todoId;
			},
			0
		);
	}

	this.updateChecked = function (id, checked)
	{
		return this._filteredUpdate(
			function (row)
			{
				return row.id == id;
			},
			function (row)
			{
				row.checked = checked;
				return row;
			},
			1
		);
	}

	this.unsetCategoryId = function (categoryId)
	{
		return this._filteredUpdate(
			function (row)
			{
				return row.categoryId == categoryId;
			},
			function (row)
			{
				row.categoryId = null;
				return row;
			},
			1
		);
	}

}