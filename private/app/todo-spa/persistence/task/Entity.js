
const TodoSpa_Persistence_Task_Entity = function (
	id,
	slug,
	todoIdArg,
	name,
	checked
)
{
	TodoSpa_Persistence_EntityAbs.call(
		this,
		id
	);

	let todoId = Number(todoIdArg);
	let categoryId = null;

	this.getSlug = function ()
	{
		return slug;
	}

	this.setSlug = function (v)
	{
		slug = v;
	}

	this.getTodoId = function ()
	{
		return todoId;
	}

	this.setTodoId = function (v)
	{
		todoId = Number(v);
	}

	this.getName = function ()
	{
		return name;
	}

	this.setName = function (v)
	{
		name = v;
	}

	this.getChecked = function ()
	{
		return checked;
	}

	this.setChecked = function (v)
	{
		checked = v;
	}

	this.hasCategoryId = function ()
	{
		return categoryId !== null;
	}

	this.getCategoryIdAfterHas = function ()
	{
		return categoryId;
	}

	this.setCategoryId = function (v)
	{
		categoryId = Number(v);
	}

	this.unsetCategoryId = function ()
	{
		categoryId = null;
	}

}