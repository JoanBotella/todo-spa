
const TodoSpa_Persistence_Settings_Persister = function (
	storageContainer
)
{
	TodoSpa_Persistence_PersisterAbs.call(
		this,
		storageContainer
	);

	this._getTableName = function ()
	{
		return 'settings';
	}

	this._buildDefaultTable = function ()
	{
		return [
			this._buildDefaultSettings()
		];
	}

		this._buildDefaultSettings = function ()
		{
			return {
				id: 0,
			};
		}

	this._buildEntityByRow = function (row)
	{
		let entity = new TodoSpa_Persistence_Settings_Entity(
			row.id,
		);

		if (row.userName !== undefined)
		{
			entity.setUserName(row.userName);
		}

		if (row.deviceName !== undefined)
		{
			entity.setDeviceName(row.deviceName);
		}

		if (row.serverUrl !== undefined)
		{
			entity.setServerUrl(row.serverUrl);
		}

		return entity;
	}

	this._buildRowByEntity = function (entity)
	{
		return {
			id: entity.getId(),
			userName: entity.hasUserName() ? entity.getUserNameAfterHas() : undefined,
			deviceName: entity.hasDeviceName() ? entity.getDeviceNameAfterHas() : undefined,
			serverUrl: entity.hasServerUrl() ? entity.getServerUrlAfterHas() : undefined,
		};
	}

}
