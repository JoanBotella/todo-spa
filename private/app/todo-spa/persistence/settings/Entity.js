
const TodoSpa_Persistence_Settings_Entity = function (
	id
)
{
	TodoSpa_Persistence_EntityAbs.call(
		this,
		id,
	);

	let userName;

	this.hasUserName = function ()
	{
		return userName !== undefined;
	}

	this.getUserNameAfterHas = function ()
	{
		return userName;
	}

	this.setUserName = function (v)
	{
		userName = v;
	}

	this.unsetUserName = function ()
	{
		userName = undefined;
	}

	let deviceName;

	this.hasDeviceName = function ()
	{
		return deviceName !== undefined;
	}

	this.getDeviceNameAfterHas = function ()
	{
		return deviceName;
	}

	this.setDeviceName = function (v)
	{
		deviceName = v;
	}

	this.unsetDeviceName = function ()
	{
		deviceName = undefined;
	}

	let serverUrl;

	this.hasServerUrl = function ()
	{
		return serverUrl !== undefined;
	}

	this.getServerUrlAfterHas = function ()
	{
		return serverUrl;
	}

	this.setServerUrl = function (v)
	{
		serverUrl = v;
	}

	this.unsetServerUrl = function ()
	{
		serverUrl = undefined;
	}

}
