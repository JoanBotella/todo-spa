
const TodoSpa_Persistence_Meta_Entity = function (
	id,
	versionArg,
	lastChangeArg
)
{
	TodoSpa_Persistence_EntityAbs.call(
		this,
		id,
	);

	let version = Number(versionArg);
	let lastChange = Number(lastChangeArg);

	this.getVersion = function ()
	{
		return version;
	}

	this.setVersion = function (v)
	{
		version = Number(v);
	}

	this.getLastChange = function ()
	{
		return lastChange;
	}

	this.setLastChange = function (v)
	{
		lastChange = Number(v);
	}

}
