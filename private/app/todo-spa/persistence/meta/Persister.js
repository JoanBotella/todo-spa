
const TodoSpa_Persistence_Meta_Persister = function (
	storageContainer
)
{
	TodoSpa_Persistence_PersisterAbs.call(
		this,
		storageContainer
	);

	this._getTableName = function ()
	{
		return 'meta';
	}

	this._buildDefaultTable = function ()
	{
		return [
			this._buildDefaultMeta()
		];
	}

		this._buildDefaultMeta = function ()
		{
			return {
				id: 0,
				version: TodoSpa_Persistence_Constant.DB_VERSION,
				lastChange: Date.now()
			};
		}

	this._buildEntityByRow = function (row)
	{
		let entity = new TodoSpa_Persistence_Meta_Entity(
			row.id,
			row.version,
			row.lastChange
		);

		return entity;
	}

	this._buildRowByEntity = function (entity)
	{
		return {
			id: entity.getId(),
			version: entity.getVersion(),
			lastChange: entity.getLastChange(),
		};
	}
}
