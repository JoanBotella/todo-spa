
const TodoSpa_Persistence_EntityAbs = function (
	idArg
)
{
	let id = Number(idArg);

	this.getId = function ()
	{
		return id;
	}

	this.setId = function (v)
	{
		id = Number(v);
	}

}