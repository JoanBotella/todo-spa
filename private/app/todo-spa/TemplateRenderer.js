
const TodoSpa_TemplateRenderer = function ()
{

	this.render = function (
		templateId,
		context
	)
	{
		let templateElement = this._cloneTemplate(templateId);
		templateElement = this._contextualize(templateElement, context);
		return document.importNode(templateElement.content, true);
	}

		this._cloneTemplate = function (templateId)
		{
			let element = document.querySelector('[data-template="' + templateId + '"]');
			let clone = document.createElement('template');
			clone.innerHTML = element.innerHTML;
			return clone;
		}

		this._contextualize = function (element, context)
		{
			if (this._isObjectEmpty(context))
			{
				return element;
			}

			let innerHtml = element.innerHTML;

			for (let key in context)
			{
				innerHtml = innerHtml.replaceAll('{{' + key + '}}', context[key]);
			}
	
			element.innerHTML = innerHtml;

			return element;
		}

			this._isObjectEmpty = function (object)
			{
				if (typeof object !== 'object')
				{
					return false;
				}

				for (let key in object)
				{
					return false;
				}
				return true;
			}

}