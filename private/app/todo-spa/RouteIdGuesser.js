
const TodoSpa_RouteIdGuesser = function (
	urlBase,
	defaultRouteId
)
{

	this.guess = function ()
	{
		let routeId = window.location.toString().substring(
			urlBase.length
		);

		if (routeId[0] == '/')
		{
			routeId = routeId.substring(1);
		}

		if (routeId == '')
		{
			routeId = defaultRouteId
		}

		return routeId;
	}

}