
const TodoSpa_ServiceContainer = function ()
{

	// TodoSpa

	this.getLinksFixer = function ()
	{
		return new TodoSpa_LinksFixer();
	}

	this.getRouteIdGuesser = function ()
	{
		return new TodoSpa_RouteIdGuesser(
			'%%VAR_URL_BASE%%',
			'home',
		);
	}

	this.getRouteIdSetter = function ()
	{
		return new TodoSpa_RouteIdSetter();
	}

	this.getApp = function ()
	{
		return new TodoSpa_App(
			this.getDatabaseMigrator(),
			this.getRouteIdGuesser(),
			this.getRouter(),
		);
	}

	this.getRoutesContainer = function ()
	{
		return new TodoSpa_RoutesContainer(
			this,
		);
	}

	let todoSpa_router;

	this.getRouter = function ()
	{
		if (todoSpa_router === undefined)
		{
			todoSpa_router = new TodoSpa_Router(
				this.getRoutesContainer(),
				this.getRouteIdSetter(),
				this.getLinksFixer(),
			);
		}
		return todoSpa_router;
	}

	this.getSlotFiller = function ()
	{
		return new TodoSpa_SlotFiller();
	}

	this.getSlugSanitizer = function ()
	{
		return new TodoSpa_SlugSanitizer();
	}

	this.getTemplateRenderer = function ()
	{
		return new TodoSpa_TemplateRenderer();
	}

	// TodoSpa_DatabaseMigrator

	this.getDatabaseMigrator = function ()
	{
		return new TodoSpa_DatabaseMigrator(
			this.getStorageContainer(),
			this.getPersistenceMetaPersister()
		);
	}

	// TodoSpa_Persister

	this.getStorageContainer = function ()
	{
		return new TodoSpa_StorageContainer();
	}

	this.getPersistenceCategoryPersister = function ()
	{
		return new TodoSpa_Persistence_Category_Persister(
			this.getStorageContainer(),
		);
	}

	this.getPersistenceTaskPersister = function ()
	{
		return new TodoSpa_Persistence_Task_Persister(
			this.getStorageContainer(),
		);
	}

	this.getPersistenceTodoPersister = function ()
	{
		return new TodoSpa_Persistence_Todo_Persister(
			this.getStorageContainer(),
		);
	}

	this.getPersistenceSettingsPersister = function ()
	{
		return new TodoSpa_Persistence_Settings_Persister(
			this.getStorageContainer(),
		);
	}

	this.getPersistenceMetaPersister = function ()
	{
		return new TodoSpa_Persistence_Meta_Persister(
			this.getStorageContainer(),
		);
	}

	// TodoSpa_UseCase

	this.getUseCaseCategoryAdd = function ()
	{
		return new TodoSpa_UseCase_Category_Add(
			this.getPersistenceCategoryPersister(),
		);
	}

	this.getUseCaseCategoryDelete = function ()
	{
		return new TodoSpa_UseCase_Category_Delete(
			this.getPersistenceCategoryPersister(),
			this.getPersistenceTaskPersister(),
		);
	}

	this.getUseCaseCategoryEdit = function ()
	{
		return new TodoSpa_UseCase_Category_Edit(
			this.getPersistenceCategoryPersister(),
		);
	}

	this.getUseCaseTaskAdd = function ()
	{
		return new TodoSpa_UseCase_Task_Add(
			this.getPersistenceTaskPersister(),
		);
	}

	this.getUseCaseTaskCheck = function ()
	{
		return new TodoSpa_UseCase_Task_Check(
			this.getPersistenceTaskPersister(),
		);
	}

	this.getUseCaseTaskDelete = function ()
	{
		return new TodoSpa_UseCase_Task_Delete(
			this.getPersistenceTaskPersister(),
		);
	}

	this.getUseCaseTaskEdit = function ()
	{
		return new TodoSpa_UseCase_Task_Edit(
			this.getPersistenceTaskPersister(),
		);
	}

	this.getUseCaseTodoAdd = function ()
	{
		return new TodoSpa_UseCase_Todo_Add(
			this.getPersistenceTodoPersister(),
		);
	}

	this.getUseCaseTodoDelete = function ()
	{
		return new TodoSpa_UseCase_Todo_Delete(
			this.getPersistenceTodoPersister(),
			this.getPersistenceTaskPersister(),
		);
	}

	this.getUseCaseTodoEdit = function ()
	{
		return new TodoSpa_UseCase_Todo_Edit(
			this.getPersistenceTodoPersister(),
		);
	}

	this.getUseCaseSettingsEdit = function ()
	{
		return new TodoSpa_UseCase_Settings_Edit(
			this.getPersistenceSettingsPersister(),
		);
	}

	// TodoSpa_Book_Front

	this.getBookFrontWidgetLayout = function ()
	{
		return new TodoSpa_Book_Front_Widget_Layout_Layout(
			this.getTemplateRenderer(),
			this.getBookFrontWidgetBreadcrumbs(),
			this.getSlotFiller(),
			this.getBookFrontPageHomeRouteIdBuilder(),
			this.getBookFrontWidgetFooter(),
		);
	}

	this.getBookFrontWidgetFooter = function ()
	{
		return new TodoSpa_Book_Front_Widget_Footer_Footer(
			this.getTemplateRenderer(),
		);
	}

	this.getBookFrontWidgetBreadcrumbs = function ()
	{
		return new TodoSpa_Book_Front_Widget_Breadcrumbs_Breadcrumbs(
			this.getTemplateRenderer(),
		);
	}

	// TodoSpa_Book_Front_Page_About

	this.getBookFrontPageAboutRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_About_Route(
			this,
		);
	}

	this.getBookFrontPageAboutRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_About_RouteIdBuilder();
	}

	this.getBookFrontPageAboutController = function ()
	{
		return new TodoSpa_Book_Front_Page_About_Controller(
			this.getBookFrontPageAboutRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageAboutWidgetMainMain(),
			this.getBookFrontPageHomeRouteIdBuilder(),
		);
	}

	this.getBookFrontPageAboutRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_About_RouteArgsBuilder();
	}

	this.getBookFrontPageAboutRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_About_RouteIdMatcher();
	}

	this.getBookFrontPageAboutWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_About_Widget_Main_Main(
			this.getTemplateRenderer(),
		);
	}

	// TodoSpa_Book_Front_Page_Settings_Show

	this.getBookFrontPageSettingsShowRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_Settings_Show_Route(
			this,
		);
	}

	this.getBookFrontPageSettingsShowRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Settings_Show_RouteIdBuilder();
	}

	this.getBookFrontPageSettingsShowController = function ()
	{
		return new TodoSpa_Book_Front_Page_Settings_Show_Controller(
			this.getBookFrontPageSettingsShowRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageSettingsShowWidgetMainMain(),
			this.getBookFrontPageHomeRouteIdBuilder(),
		);
	}

	this.getBookFrontPageSettingsShowRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Settings_Show_RouteArgsBuilder();
	}

	this.getBookFrontPageSettingsShowRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_Settings_Show_RouteIdMatcher();
	}

	this.getBookFrontPageSettingsShowWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_Settings_Show_Widget_Main_Main(
			this.getTemplateRenderer(),
			this.getPersistenceSettingsPersister(),
			this.getBookFrontPageSettingsEditRouteIdBuilder(),
		);
	}

	// TodoSpa_Book_Front_Page_Settings_Edit

	this.getBookFrontPageSettingsEditRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_Settings_Edit_Route(
			this,
		);
	}

	this.getBookFrontPageSettingsEditRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Settings_Edit_RouteIdBuilder();
	}

	this.getBookFrontPageSettingsEditController = function ()
	{
		return new TodoSpa_Book_Front_Page_Settings_Edit_Controller(
			this.getBookFrontPageSettingsEditRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageSettingsEditWidgetMainMain(),
			this.getBookFrontPageHomeRouteIdBuilder(),
			this.getBookFrontPageSettingsShowRouteIdBuilder(),
		);
	}

	this.getBookFrontPageSettingsEditRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Settings_Edit_RouteArgsBuilder();
	}

	this.getBookFrontPageSettingsEditRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_Settings_Edit_RouteIdMatcher();
	}

	this.getBookFrontPageSettingsEditWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_Settings_Edit_Widget_Main_Main(
			this.getTemplateRenderer(),
			this.getPersistenceSettingsPersister(),
			this.getUseCaseSettingsEdit(),
			this.getBookFrontPageSettingsShowRouteIdBuilder(),
			this.getRouter(),
		);
	}

	// TodoSpa_Book_Front_Page_Home

	this.getBookFrontPageHomeRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_Home_Route(
			this,
		);
	}

	this.getBookFrontPageHomeRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Home_RouteIdBuilder();
	}

	this.getBookFrontPageHomeController = function ()
	{
		return new TodoSpa_Book_Front_Page_Home_Controller(
			this.getBookFrontPageHomeRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageHomeWidgetMainMain(),
		);
	}

	this.getBookFrontPageHomeRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Home_RouteArgsBuilder();
	}

	this.getBookFrontPageHomeRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_Home_RouteIdMatcher();
	}

	this.getBookFrontPageHomeWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_Home_Widget_Main_Main(
			this.getTemplateRenderer(),
			this.getBookFrontPageCategoryListRouteIdBuilder(),
			this.getBookFrontPageTodoListRouteIdBuilder(),
			this.getBookFrontPageDataMenuRouteIdBuilder(),
			this.getBookFrontPageSettingsShowRouteIdBuilder(),
			this.getBookFrontPageAboutRouteIdBuilder(),
		);
	}

	// TodoSpa_Book_Front_Page_NotFound

	this.getBookFrontPageNotFoundRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_NotFound_Route(
			this,
		);
	}

	this.getBookFrontPageNotFoundRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_NotFound_RouteIdBuilder();
	}

	this.getBookFrontPageNotFoundController = function ()
	{
		return new TodoSpa_Book_Front_Page_NotFound_Controller(
			this.getBookFrontPageNotFoundRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageNotFoundWidgetMainMain(),
			this.getBookFrontPageHomeRouteIdBuilder(),
		);
	}

	this.getBookFrontPageNotFoundRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_NotFound_RouteArgsBuilder();
	}

	this.getBookFrontPageNotFoundRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_NotFound_RouteIdMatcher();
	}

	this.getBookFrontPageNotFoundWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_NotFound_Widget_Main_Main(
			this.getTemplateRenderer(),
		);
	}

	// TodoSpa_Book_Front_Page_Category_Add

	this.getBookFrontPageCategoryAddRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_Add_Route(
			this,
		);
	}

	this.getBookFrontPageCategoryAddRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_Add_RouteIdBuilder();
	}

	this.getBookFrontPageCategoryAddController = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_Add_Controller(
			this.getBookFrontPageCategoryAddRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageCategoryAddWidgetMainMain(),
			this.getBookFrontPageHomeRouteIdBuilder(),
			this.getBookFrontPageCategoryListRouteIdBuilder(),
		);
	}

	this.getBookFrontPageCategoryAddRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_Add_RouteArgsBuilder();
	}

	this.getBookFrontPageCategoryAddRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_Add_RouteIdMatcher();
	}

	this.getBookFrontPageCategoryAddWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_Add_Widget_Main_Main(
			this.getTemplateRenderer(),
			this.getUseCaseCategoryAdd(),
			this.getBookFrontPageCategoryListRouteIdBuilder(),
			this.getRouter(),
			this.getSlugSanitizer(),
		);
	}

	// TodoSpa_Book_Front_Page_Category_Edit

	this.getBookFrontPageCategoryEditRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_Edit_Route(
			this,
		);
	}

	this.getBookFrontPageCategoryEditRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_Edit_RouteIdBuilder();
	}
	
	this.getBookFrontPageCategoryEditController = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_Edit_Controller(
			this.getBookFrontPageCategoryEditRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageCategoryEditWidgetMainMain(),
			this.getBookFrontPageHomeRouteIdBuilder(),
			this.getBookFrontPageCategoryListRouteIdBuilder(),
			this.getPersistenceCategoryPersister(),
		);
	}

	this.getBookFrontPageCategoryEditRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_Edit_RouteArgsBuilder();
	}

	this.getBookFrontPageCategoryEditRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_Edit_RouteIdMatcher();
	}

	this.getBookFrontPageCategoryEditWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_Edit_Widget_Main_Main(
			this.getTemplateRenderer(),
			this.getUseCaseCategoryEdit(),
			this.getBookFrontPageCategoryListRouteIdBuilder(),
			this.getRouter(),
			this.getSlugSanitizer(),
		);
	}

	// TodoSpa_Book_Front_Page_Category_List

	this.getBookFrontPageCategoryListRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_List_Route(
			this,
		);
	}

	this.getBookFrontPageCategoryListRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_List_RouteIdBuilder();
	}

	this.getBookFrontPageCategoryListController = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_List_Controller(
			this.getBookFrontPageCategoryListRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageCategoryListWidgetMainMain(),
			this.getBookFrontPageHomeRouteIdBuilder(),
		);
	}

	this.getBookFrontPageCategoryListRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_List_RouteArgsBuilder();
	}

	this.getBookFrontPageCategoryListRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_List_RouteIdMatcher();
	}

	this.getBookFrontPageCategoryListWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_Category_List_Widget_Main_Main(
			this.getTemplateRenderer(),
			this.getPersistenceCategoryPersister(),
			this.getBookFrontPageCategoryAddRouteIdBuilder(),
			this.getUseCaseCategoryDelete(),
			this.getBookFrontPageCategoryEditRouteIdBuilder(),
		);
	}

	// TodoSpa_Book_Front_Page_Data_Edit

	this.getBookFrontPageDataEditRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Edit_Route(
			this,
		);
	}

	this.getBookFrontPageDataEditRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Edit_RouteIdBuilder();
	}
	
	this.getBookFrontPageDataEditController = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Edit_Controller(
			this.getBookFrontPageDataEditRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageDataEditWidgetMainMain(),
			this.getBookFrontPageHomeRouteIdBuilder(),
			this.getBookFrontPageDataMenuRouteIdBuilder(),
		);
	}

	this.getBookFrontPageDataEditRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Edit_RouteArgsBuilder();
	}

	this.getBookFrontPageDataEditRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Edit_RouteIdMatcher();
	}

	this.getBookFrontPageDataEditWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Edit_Widget_Main_Main(
			this.getTemplateRenderer(),
			this.getBookFrontPageHomeRouteIdBuilder(),
			this.getRouter(),
			this.getStorageContainer(),
			this.getDatabaseMigrator()
		);
	}

	// TodoSpa_Book_Front_Page_Data_Upload

	this.getBookFrontPageDataUploadRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Upload_Route(
			this,
		);
	}

	this.getBookFrontPageDataUploadRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Upload_RouteIdBuilder();
	}
	
	this.getBookFrontPageDataUploadController = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Upload_Controller(
			this.getBookFrontPageDataUploadRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageDataUploadWidgetMainMain(),
			this.getBookFrontPageHomeRouteIdBuilder(),
			this.getBookFrontPageDataMenuRouteIdBuilder(),
		);
	}

	this.getBookFrontPageDataUploadRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Upload_RouteArgsBuilder();
	}

	this.getBookFrontPageDataUploadRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Upload_RouteIdMatcher();
	}

	this.getBookFrontPageDataUploadWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Upload_Widget_Main_Main(
			this.getTemplateRenderer(),
			this.getPersistenceMetaPersister(),
			this.getPersistenceSettingsPersister(),
			this.getStorageContainer()
		);
	}

	// TodoSpa_Book_Front_Page_Data_Download

	this.getBookFrontPageDataDownloadRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Download_Route(
			this,
		);
	}

	this.getBookFrontPageDataDownloadRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Download_RouteIdBuilder();
	}
	
	this.getBookFrontPageDataDownloadController = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Download_Controller(
			this.getBookFrontPageDataDownloadRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageDataDownloadWidgetMainMain(),
			this.getBookFrontPageHomeRouteIdBuilder(),
			this.getBookFrontPageDataMenuRouteIdBuilder(),
		);
	}

	this.getBookFrontPageDataDownloadRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Download_RouteArgsBuilder();
	}

	this.getBookFrontPageDataDownloadRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Download_RouteIdMatcher();
	}

	this.getBookFrontPageDataDownloadWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Download_Widget_Main_Main(
			this.getTemplateRenderer(),
			this.getPersistenceMetaPersister(),
			this.getPersistenceSettingsPersister(),
			this.getStorageContainer(),
			this.getDatabaseMigrator()
		);
	}

	// TodoSpa_Book_Front_Page_Data_Menu

	this.getBookFrontPageDataMenuRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Menu_Route(
			this,
		);
	}

	this.getBookFrontPageDataMenuRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Menu_RouteIdBuilder();
	}
	
	this.getBookFrontPageDataMenuController = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Menu_Controller(
			this.getBookFrontPageDataMenuRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageDataMenuWidgetMainMain(),
			this.getBookFrontPageHomeRouteIdBuilder(),
		);
	}

	this.getBookFrontPageDataMenuRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Menu_RouteArgsBuilder();
	}

	this.getBookFrontPageDataMenuRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Menu_RouteIdMatcher();
	}

	this.getBookFrontPageDataMenuWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_Data_Menu_Widget_Main_Main(
			this.getTemplateRenderer(),
			this.getBookFrontPageDataDownloadRouteIdBuilder(),
			this.getBookFrontPageDataUploadRouteIdBuilder(),
			this.getBookFrontPageDataEditRouteIdBuilder(),
		);
	}

	// TodoSpa_Book_Front_Page_Todo_Add

	this.getBookFrontPageTodoAddRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_Add_Route(
			this,
		);
	}

	this.getBookFrontPageTodoAddRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_Add_RouteIdBuilder();
	}

	this.getBookFrontPageTodoAddController = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_Add_Controller(
			this.getBookFrontPageTodoAddRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageTodoAddWidgetMainMain(),
			this.getBookFrontPageHomeRouteIdBuilder(),
			this.getBookFrontPageTodoListRouteIdBuilder(),
		);
	}

	this.getBookFrontPageTodoAddRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_Add_RouteArgsBuilder();
	}

	this.getBookFrontPageTodoAddRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_Add_RouteIdMatcher();
	}

	this.getBookFrontPageTodoAddWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_Add_Widget_Main_Main(
			this.getTemplateRenderer(),
			this.getUseCaseTodoAdd(),
			this.getBookFrontPageTodoListRouteIdBuilder(),
			this.getRouter(),
			this.getSlugSanitizer(),
		);
	}

	// TodoSpa_Book_Front_Page_Todo_Edit

	this.getBookFrontPageTodoEditRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_Edit_Route(
			this,
		);
	}

	this.getBookFrontPageTodoEditRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_Edit_RouteIdBuilder();
	}
	
	this.getBookFrontPageTodoEditController = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_Edit_Controller(
			this.getBookFrontPageTodoEditRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageTodoEditWidgetMainMain(),
			this.getBookFrontPageHomeRouteIdBuilder(),
			this.getBookFrontPageTodoListRouteIdBuilder(),
			this.getPersistenceTodoPersister(),
		);
	}

	this.getBookFrontPageTodoEditRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_Edit_RouteArgsBuilder();
	}

	this.getBookFrontPageTodoEditRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_Edit_RouteIdMatcher();
	}

	this.getBookFrontPageTodoEditWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_Edit_Widget_Main_Main(
			this.getTemplateRenderer(),
			this.getUseCaseTodoEdit(),
			this.getBookFrontPageTodoListRouteIdBuilder(),
			this.getRouter(),
			this.getSlugSanitizer(),
		);
	}

	// TodoSpa_Book_Front_Page_Todo_List

	this.getBookFrontPageTodoListRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_List_Route(
			this,
		);
	}

	this.getBookFrontPageTodoListRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_List_RouteIdBuilder();
	}

	this.getBookFrontPageTodoListController = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_List_Controller(
			this.getBookFrontPageTodoListRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageTodoListWidgetMainMain(),
			this.getBookFrontPageHomeRouteIdBuilder(),
		);
	}

	this.getBookFrontPageTodoListRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_List_RouteArgsBuilder();
	}

	this.getBookFrontPageTodoListRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_List_RouteIdMatcher();
	}

	this.getBookFrontPageTodoListWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_Todo_List_Widget_Main_Main(
			this.getTemplateRenderer(),
			this.getPersistenceTodoPersister(),
			this.getBookFrontPageTaskListRouteIdBuilder(),
			this.getBookFrontPageTodoAddRouteIdBuilder(),
			this.getUseCaseTodoDelete(),
			this.getBookFrontPageTodoEditRouteIdBuilder(),
		);
	}

	// TodoSpa_Book_Front_Page_Task_Add

	this.getBookFrontPageTaskAddRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_Add_Route(
			this,
		);
	}

	this.getBookFrontPageTaskAddRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_Add_RouteIdBuilder();
	}
	
	this.getBookFrontPageTaskAddController = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_Add_Controller(
			this.getBookFrontPageTaskAddRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageTaskAddWidgetMainMain(),
			this.getBookFrontPageHomeRouteIdBuilder(),
			this.getBookFrontPageTodoListRouteIdBuilder(),
			this.getBookFrontPageTaskListRouteIdBuilder(),
			this.getPersistenceTodoPersister(),
		);
	}

	this.getBookFrontPageTaskAddRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_Add_RouteArgsBuilder();
	}

	this.getBookFrontPageTaskAddRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_Add_RouteIdMatcher();
	}

	this.getBookFrontPageTaskAddWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_Add_Widget_Main_Main(
			this.getTemplateRenderer(),
			this.getPersistenceCategoryPersister(),
			this.getUseCaseTaskAdd(),
			this.getBookFrontPageTaskListRouteIdBuilder(),
			this.getRouter(),
			this.getSlugSanitizer(),
		);
	}

	// TodoSpa_Book_Front_Page_Task_Edit

	this.getBookFrontPageTaskEditRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_Edit_Route(
			this,
		);
	}

	this.getBookFrontPageTaskEditRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_Edit_RouteIdBuilder();
	}
	
	this.getBookFrontPageTaskEditController = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_Edit_Controller(
			this.getBookFrontPageTaskEditRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageTaskEditWidgetMainMain(),
			this.getBookFrontPageHomeRouteIdBuilder(),
			this.getBookFrontPageTodoListRouteIdBuilder(),
			this.getBookFrontPageTaskListRouteIdBuilder(),
			this.getPersistenceTodoPersister(),
			this.getPersistenceTaskPersister(),
		);
	}

	this.getBookFrontPageTaskEditRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_Edit_RouteArgsBuilder();
	}

	this.getBookFrontPageTaskEditRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_Edit_RouteIdMatcher();
	}

	this.getBookFrontPageTaskEditWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_Edit_Widget_Main_Main(
			this.getTemplateRenderer(),
			this.getPersistenceTodoPersister(),
			this.getPersistenceCategoryPersister(),
			this.getUseCaseTaskEdit(),
			this.getBookFrontPageTaskListRouteIdBuilder(),
			this.getRouter(),
			this.getSlugSanitizer(),
		);
	}

	// TodoSpa_Book_Front_Page_Task_List

	this.getBookFrontPageTaskListRoute = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_List_Route(
			this,
		);
	}

	this.getBookFrontPageTaskListRouteIdBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_List_RouteIdBuilder();
	}
	
	this.getBookFrontPageTaskListController = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_List_Controller(
			this.getBookFrontPageTaskListRouteArgsBuilder(),
			this.getSlotFiller(),
			this.getBookFrontWidgetLayout(),
			this.getBookFrontPageTaskListWidgetMainMain(),
			this.getBookFrontPageHomeRouteIdBuilder(),
			this.getBookFrontPageTodoListRouteIdBuilder(),
			this.getPersistenceTodoPersister(),
		);
	}

	this.getBookFrontPageTaskListRouteArgsBuilder = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_List_RouteArgsBuilder();
	}

	this.getBookFrontPageTaskListRouteIdMatcher = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_List_RouteIdMatcher();
	}

	this.getBookFrontPageTaskListWidgetMainMain = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_List_Widget_Main_Main(
			this.getTemplateRenderer(),
			this.getPersistenceTaskPersister(),
			this.getPersistenceCategoryPersister(),
			this.getBookFrontPageTaskAddRouteIdBuilder(),
			this.getUseCaseTaskDelete(),
			this.getBookFrontPageTaskEditRouteIdBuilder(),
			this.getUseCaseTaskCheck(),
			this.getBookFrontPageTaskListWidgetMainTasksSorter(),
		);
	}

	this.getBookFrontPageTaskListWidgetMainTasksSorter = function ()
	{
		return new TodoSpa_Book_Front_Page_Task_List_Widget_Main_TasksSorter(
			this.getPersistenceCategoryPersister(),
		);
	}

}
