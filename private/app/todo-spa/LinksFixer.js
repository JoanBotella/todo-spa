
const TodoSpa_LinksFixer = function ()
{

	this.fix = function (router)
	{
		const linkElements = document.querySelectorAll('a');
		for (let index = 0; index < linkElements.length; index++)
		{
			linkElements[index].addEventListener(
				'click',
				function (event)
				{
					const href = event.target.getAttribute('href');

					if (
						href.startsWith('http://')
						|| href.startsWith('https://')
						|| href.startsWith('/')
					)
					{
						return true;
					}

					event.preventDefault();

					router.route(href);
				}
			);
		}
	}

}