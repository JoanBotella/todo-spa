
const TodoSpa_SlugSanitizer = function (
)
{

	const REPLACES = [
		[' ', '-'],
		['\'', ''],
		['"', ''],
		['`', ''],
		['?', ''],
		['&', 'and'],
		['\\', ''],
		['/', ''],
		['=', ''],
		['#', ''],
		['á', 'a'],
		['é', 'e'],
		['í', 'i'],
		['ó', 'o'],
		['ú', 'u'],
		['à', 'a'],
		['è', 'e'],
		['ò', 'o'],
		['ñ', 'n'],
		['ç', 'c'],
	];

	this.sanitize = function (text)
	{
		let r = text.trim();
		r = r.toLowerCase();
		for (let index = 0; index < REPLACES.length; index++)
		{
			let pair = REPLACES[index];
			r = r.replaceAll(
				pair[0],
				pair[1]
			);
		}

		r = encodeURIComponent(r);
		return r;
	}

}