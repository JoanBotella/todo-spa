
const TodoSpa_SlotFiller = function ()
{

	this.fill = function (rootElement, slotName, childElement)
	{
		let slotElement = rootElement.querySelector('[data-slot="' + slotName + '"]');
		slotElement.innerHTML = '';
		slotElement.appendChild(childElement);
		return rootElement;
	}

}