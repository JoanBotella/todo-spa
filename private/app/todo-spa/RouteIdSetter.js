
const TodoSpa_RouteIdSetter = function ()
{

	this.set = function (routeId)
	{
		const bodyElement = document.querySelector('body');
		bodyElement.setAttribute('data-route_id', routeId);
	}

}