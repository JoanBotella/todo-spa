
const TodoSpa_Router = function (
	routesContainer,
	routeIdSetter,
	linksFixer
)
{

	let firstRoute = true;

	this.setupOnPopState = function ()
	{
		const that = this;

		window.onpopstate = function (event)
		{
			that.routeBack();
		}
	}

	this.route = function (routeId)
	{
		this._replaceOrPushState(routeId);

		this._searchControllerAndRun(routeId);
	}

		this._replaceOrPushState = function (routeId)
		{
			const stateData = {
				routeId: routeId
			};

			if (firstRoute)
			{
				window.history.replaceState(stateData, '', routeId);

				firstRoute = false;
				return;
			}

			window.history.pushState(stateData, '', routeId);
		}

		this._searchControllerAndRun = function (routeId)
		{
			const routes = routesContainer.get();

			for (let index = 0; index < routes.length; index++)
			{
				let route = routes[index];
	
				if (route.getRouteIdMatcher().match(routeId))
				{
					routeIdSetter.set(routeId);
			
					route.getController().run(routeId);
	
					linksFixer.fix(this);
	
					return;
				}
			}
	
			throw Error('Matching controller not found for routeId ' + routeId);
		}

	this.routeBack = function ()
	{
		const routeId = window.history.state.routeId;
		this._searchControllerAndRun(routeId);
	}

};
