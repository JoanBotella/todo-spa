#! /bin/bash



# Load the configuration

if [ $# -gt 0 ]
then
	ENV_FILE="$1"
else
	ENV_FILE="../env/dev.sh"
fi

if [ ! -f "$ENV_FILE" ]
then
	echo "Environment file \"$ENV_FILE\" not found. You can use ../env/example.sh a as base for it." 1>&2
	exit 1
fi

source "$ENV_FILE"



# Clean the public dir

rm -rf "$PUBLIC_DIR"

mkdir "$PUBLIC_DIR"



# Functions

function replaceVariablesInFile ()
{
	FILE="$1"

	sed -i "s,%%VAR_PROTOCOL%%,$VAR_PROTOCOL,g" "$FILE"
	sed -i "s,%%VAR_DOMAIN%%,$VAR_DOMAIN,g" "$FILE"
	sed -i "s,%%VAR_PORT%%,$VAR_PORT,g" "$FILE"
	sed -i "s,%%VAR_PATH%%,$VAR_PATH,g" "$FILE"
	sed -i "s,%%VAR_URL_BASE%%,$VAR_URL_BASE,g" "$FILE"
}



# HTML

HTML_FILE="$PUBLIC_DIR/index.html"

cat "$PRIVATE_APP_DIR/base/html/head.html" > "$HTML_FILE"

for file in `find "$PROJECT_DIR/private/app" -name *.html`
do
	if
		[ $file != "$PRIVATE_APP_DIR/base/html/head.html" ] \
		&& [ $file != "$PRIVATE_APP_DIR/base/html/tail.html" ]
	then
		cat "$file" >> "$HTML_FILE"
	fi
done

cat "$PRIVATE_APP_DIR/base/html/tail.html" >> "$HTML_FILE"

replaceVariablesInFile "$HTML_FILE"



# CSS

mkdir "$PUBLIC_DIR/css"

CSS_FILE="$PUBLIC_DIR/css/style.css"

cat "$PRIVATE_APP_DIR/base/css/head.css" > "$CSS_FILE"

for file in `find "$PROJECT_DIR/private/app" -name *.css`
do
	if
		[ $file != "$PRIVATE_APP_DIR/base/css/head.css" ]
	then
		cat "$file" >> "$CSS_FILE"
	fi
done

replaceVariablesInFile "$CSS_FILE"



# JS

mkdir "$PUBLIC_DIR/js"

JS_FILE="$PUBLIC_DIR/js/script.js"

cat "$PRIVATE_APP_DIR/base/js/head.js" > "$JS_FILE"

for file in `find "$PROJECT_DIR/private/app" -name *.js`
do
	if
		[ $file != "$PRIVATE_APP_DIR/base/js/head.js" ] \
		&& [ $file != "$PRIVATE_APP_DIR/base/js/tail.js" ]
	then
		cat "$file" >> "$JS_FILE"
	fi
done

cat "$PRIVATE_APP_DIR/base/js/tail.js" >> "$JS_FILE"

replaceVariablesInFile "$JS_FILE"



# ASSET

mkdir "$PUBLIC_DIR/asset"

cp "$PRIVATE_APP_DIR/base/asset/"* "$PUBLIC_DIR/asset"
